package com.getjavajob.training.algo08.init.gordienkov.lesson01;

import static com.getjavajob.training.algo08.util.ConsoleHelper.getIntNumber;
import static com.getjavajob.training.algo08.util.ConsoleHelper.println;

public class Task07 {
    public static void main(String[] args) {
        println("Enter first number");
        int a = getIntNumber();
        println("Enter second number");
        int b = getIntNumber();
        println(swapValuesOfVars1(a, b));
        println(swapValuesOfVars2(a, b));
        println(swapValuesOfVars3(a, b));
        println(swapValuesOfVars4(a, b));
    }

    static int swapValuesOfVars1(int a, int b) {
        a ^= b;
        b ^= a;
        a ^= b;
        return a;
    }

    static int swapValuesOfVars2(int a, int b) {
        a += b;
        b = a - b;
        a -= b;
        return a;
    }

    static int swapValuesOfVars3(int a, int b) {
        a *= b;
        b = a / b;
        a /= b;
        return a;
    }

    static int swapValuesOfVars4(int a, int b) {
        a &= b;
        b = a | b;
        a |= b;
        return a;
    }

}
