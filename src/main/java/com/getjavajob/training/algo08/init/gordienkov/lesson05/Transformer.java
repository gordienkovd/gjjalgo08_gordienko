package com.getjavajob.training.algo08.init.gordienkov.lesson05;

public interface Transformer<I, O> {
    O transformCollection(I input);
}
