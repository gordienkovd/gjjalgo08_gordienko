package com.getjavajob.training.algo08.init.gordienkov.lesson05;

import org.junit.Test;

import java.util.ArrayDeque;
import java.util.Deque;

import static org.junit.Assert.*;

public class DequeTest {
    @Test
    public void addFirst() {
        Deque<Integer> actual = new ArrayDeque<>();
        actual.addFirst(1);
        actual.addFirst(2);
        Integer[] expected = new Integer[]{2, 1};
        assertArrayEquals(expected, actual.toArray());
    }

    @Test
    public void addLast() {
        Deque<Integer> actual = new ArrayDeque<>();
        actual.addLast(1);
        actual.addLast(2);
        Integer[] expected = new Integer[]{1, 2};
        assertArrayEquals(expected, actual.toArray());
    }

    @Test
    public void offerFirst() {
        Deque<Integer> actual = new ArrayDeque<>();
        actual.offerFirst(1);
        actual.offerFirst(2);
        Integer[] expected = new Integer[]{2, 1};
        assertArrayEquals(expected, actual.toArray());
    }

    @Test
    public void offerLast() {
        Deque<Integer> actual = new ArrayDeque<>();
        actual.offerLast(1);
        actual.offerLast(2);
        Integer[] expected = new Integer[]{1, 2};
        assertArrayEquals(expected, actual.toArray());
    }

    @Test
    public void removeFirst() {
        Deque<Integer> actual = new ArrayDeque<>();
        actual.addFirst(1);
        actual.addFirst(2);
        actual.removeFirst();
        Integer[] expected = new Integer[]{1};
        assertArrayEquals(expected, actual.toArray());
    }

    @Test
    public void removeLast() {
        Deque<Integer> actual = new ArrayDeque<>();
        actual.addFirst(1);
        actual.addFirst(2);
        actual.removeLast();
        Integer[] expected = new Integer[]{2};
        assertArrayEquals(expected, actual.toArray());
    }

    @Test
    public void pollFirst() {
        Deque<Integer> actual = new ArrayDeque<>();
        actual.addFirst(1);
        actual.addFirst(2);
        actual.pollFirst();
        Integer[] expected = new Integer[]{1};
        assertArrayEquals(expected, actual.toArray());
    }

    @Test
    public void pollLast() {
        Deque<Integer> actual = new ArrayDeque<>();
        actual.addFirst(1);
        actual.addFirst(2);
        actual.pollLast();
        Integer[] expected = new Integer[]{2};
        assertArrayEquals(expected, actual.toArray());
    }

    @Test
    public void getFirst() {
        Deque<String> actual = new ArrayDeque<>();
        actual.addFirst("a");
        actual.addFirst("b");
        assertEquals("b", actual.getFirst());
    }

    @Test
    public void getLast() {
        Deque<String> actual = new ArrayDeque<>();
        actual.addFirst("a");
        actual.addFirst("b");
        assertEquals("a", actual.getLast());
    }

    @Test
    public void peekFirst() {
        Deque<String> actual = new ArrayDeque<>();
        actual.addFirst("a");
        actual.addFirst("b");
        assertEquals("b", actual.peekFirst());
    }

    @Test
    public void peekLast() {
        Deque<String> actual = new ArrayDeque<>();
        actual.addFirst("a");
        actual.addFirst("b");
        assertEquals("a", actual.peekLast());
    }

    @Test
    public void removeFirstOccurrence() {
        Deque<String> actual = new ArrayDeque<>();
        actual.addFirst("a");
        actual.addFirst("b");
        actual.addFirst("a");
        actual.addFirst("c");
        actual.removeFirstOccurrence("a");
        String[] expected = new String[]{"c", "b", "a"};
        assertArrayEquals(expected, actual.toArray());
    }

    @Test
    public void removeLastOccurrence() {
        Deque<String> actual = new ArrayDeque<>();
        actual.addFirst("a");
        actual.addFirst("b");
        actual.addFirst("a");
        actual.addFirst("c");
        actual.removeLastOccurrence("a");
        String[] expected = new String[]{"c", "a", "b"};
        assertArrayEquals(expected, actual.toArray());
    }

    @Test
    public void add() {
        Deque<String> actual = new ArrayDeque<>();
        actual.add("a");
        actual.add("b");
        actual.add("c");
        String[] expected = new String[]{"a", "b", "c"};
        assertArrayEquals(expected, actual.toArray());
    }

    @Test
    public void offer() {
        Deque<String> actual = new ArrayDeque<>();
        actual.offer("a");
        actual.offer("b");
        actual.offer("c");
        String[] expected = new String[]{"a", "b", "c"};
        assertArrayEquals(expected, actual.toArray());
    }

    @Test
    public void remove() {
        Deque<String> actual = new ArrayDeque<>();
        actual.add("a");
        actual.add("b");
        actual.add("c");
        actual.remove();
        String[] expected = new String[]{"b", "c"};
        assertArrayEquals(expected, actual.toArray());
    }

    @Test
    public void poll() {
        Deque<String> actual = new ArrayDeque<>();
        actual.add("a");
        actual.add("b");
        actual.add("c");
        actual.poll();
        String[] expected = new String[]{"b", "c"};
        assertArrayEquals(expected, actual.toArray());
    }

    @Test
    public void element() {
        Deque<String> actual = new ArrayDeque<>();
        actual.add("a");
        actual.add("b");
        actual.add("c");
        assertEquals("a", actual.element());
    }

    @Test
    public void peek() {
        Deque<String> actual = new ArrayDeque<>();
        actual.add("a");
        actual.add("b");
        actual.add("c");
        assertEquals("a", actual.peek());
    }

    @Test
    public void push() {
        Deque<String> actual = new ArrayDeque<>();
        actual.push("a");
        actual.push("b");
        actual.push("c");
        String[] expected = new String[]{"c", "b", "a"};
        assertArrayEquals(expected, actual.toArray());
    }

    @Test
    public void pop() {
        Deque<String> actual = new ArrayDeque<>();
        actual.push("a");
        actual.push("b");
        actual.pop();
        String[] expected = new String[]{"a"};
        assertArrayEquals(expected, actual.toArray());
    }

    @Test
    public void removeCollectionMethod() {
        Deque<String> actual = new ArrayDeque<>();
        actual.push("a");
        actual.push("b");
        actual.remove("b");
        String[] expected = new String[]{"a"};
        assertArrayEquals(expected, actual.toArray());
    }

    @Test
    public void containsTrue() {
        Deque<String> actual = new ArrayDeque<>();
        actual.push("a");
        actual.push("b");
        assertTrue(actual.contains("a"));
    }

    @Test
    public void containsFalse() {
        Deque<String> actual = new ArrayDeque<>();
        actual.push("a");
        actual.push("b");
        assertFalse(actual.contains("c"));
    }

    @Test
    public void size() {
        Deque<String> actual = new ArrayDeque<>();
        actual.push("a");
        actual.push("b");
        assertEquals(2, actual.size());
    }
}