package com.getjavajob.training.algo08.init.gordienkov.lesson03;

import com.getjavajob.training.algo08.util.StopWatch;

import java.util.ArrayList;
import java.util.List;

import static com.getjavajob.training.algo08.util.ConsoleHelper.println;

public class DynamicArrayPerformanceTest {
    public static void main(String[] args) {
        addToBeginningDynamicArray();
        addToBeginningArrayList();
        addToMiddleDynamicArray();
        addToMiddleArrayList();
        addToEndDynamicArray();
        addEndArrayList();
        removeFromBeginningDynamicArray();
        removeFromBeginningArrayList();
        removeFromMiddleDynamicArray();
        removeFromMiddleArrayList();
        removeFromEndDynamicArray();
        removeFromEndArrayList();
    }

    private static void addToBeginningDynamicArray() {
        long start = StopWatch.start();
        DynamicArray da = new DynamicArray(400000);
        for (int i = 0; i < 400000; i++) {
            da.add(0, 1);
        }
        println("DynamicArray.add(): " + StopWatch.getElapsedTime(start) + " ms");
    }

    private static void addToBeginningArrayList() {
        long start1 = StopWatch.start();
        List<Object> al = new ArrayList<>(400000);
        for (int i = 0; i < 400000; i++) {
            al.add(0, 1);
        }
        println("ArrayList.add(): " + StopWatch.getElapsedTime(start1) + " ms");
    }

    private static void addToMiddleDynamicArray() {
        long start = StopWatch.start();
        DynamicArray da = new DynamicArray(400000);
        for (int i = 0; i < 400000; i++) {
            da.add(da.size() / 2, 1);
        }
        println("DynamicArray.add(): " + StopWatch.getElapsedTime(start) + " ms");
    }

    private static void addToMiddleArrayList() {
        long start1 = StopWatch.start();
        List<Object> al = new ArrayList<>(400000);
        for (int i = 0; i < 400000; i++) {
            al.add(al.size() / 2, 1);
        }
        println("ArrayList.add(): " + StopWatch.getElapsedTime(start1) + " ms");
    }

    private static void addToEndDynamicArray() {
        long start = StopWatch.start();
        DynamicArray da = new DynamicArray(1000000000);
        for (int i = 0; i < 1000000000; i++) {
            da.add(1);
        }
        println("DynamicArray.add(): " + StopWatch.getElapsedTime(start) + " ms");
    }

    private static void addEndArrayList() {
        long start1 = StopWatch.start();
        List<Object> al = new ArrayList<>(1000000000);
        for (int i = 0; i < 1000000000; i++) {
            al.add(1);
        }
        println("ArrayList.add(): " + StopWatch.getElapsedTime(start1) + " ms");
    }

    private static void removeFromBeginningDynamicArray() {
        DynamicArray da = new DynamicArray(400000);
        for (int i = 0; i < 400000; i++) {
            da.add(1);
        }
        long start = StopWatch.start();
        for (int i = 0; i < 400000; i++) {
            da.remove(0);
        }
        println("DynamicArray.remove(): " + StopWatch.getElapsedTime(start) + " ms");
    }

    private static void removeFromBeginningArrayList() {
        DynamicArray al = new DynamicArray(400000);
        for (int i = 0; i < 400000; i++) {
            al.add(1);
        }
        long start = StopWatch.start();
        for (int i = 0; i < 400000; i++) {
            al.remove(0);
        }
        println("ArrayList.remove(): " + StopWatch.getElapsedTime(start) + " ms");
    }

    private static void removeFromMiddleDynamicArray() {
        DynamicArray da = new DynamicArray(400000);
        for (int i = 0; i < 400000; i++) {
            da.add(1);
        }
        long start = StopWatch.start();
        for (int i = 0; i < 190000; i++) {
            da.remove(da.size() / 2);
        }
        println("DynamicArray.remove(): " + StopWatch.getElapsedTime(start) + " ms");
    }

    private static void removeFromMiddleArrayList() {
        DynamicArray al = new DynamicArray(400000);
        for (int i = 0; i < 400000; i++) {
            al.add(1);
        }
        long start = StopWatch.start();
        for (int i = 0; i < 190000; i++) {
            al.remove(al.size() / 2);
        }
        println("ArrayList.remove(): " + StopWatch.getElapsedTime(start) + " ms");
    }

    private static void removeFromEndDynamicArray() {
        DynamicArray da = new DynamicArray(2147000000);
        for (int i = 0; i < 2147000000; i++) {
            da.add(1);
        }
        long start = StopWatch.start();
        for (int i = 0; i < 2147000000; i++) {
            da.remove(da.size() - 1);
        }
        println("DynamicArray.remove(): " + StopWatch.getElapsedTime(start) + " ms");
    }

    private static void removeFromEndArrayList() {
        DynamicArray al = new DynamicArray(2147000000);
        for (int i = 0; i < 2147000000; i++) {
            al.add(1);
        }
        long start = StopWatch.start();
        for (int i = 0; i < 2147000000; i++) {
            al.remove(al.size() - 1);
        }
        println("ArrayList.remove(): " + StopWatch.getElapsedTime(start) + " ms");
    }
}
