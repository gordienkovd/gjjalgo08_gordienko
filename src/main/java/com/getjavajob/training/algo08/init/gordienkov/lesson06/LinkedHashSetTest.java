package com.getjavajob.training.algo08.init.gordienkov.lesson06;


import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedHashSet;
import java.util.Set;

public class LinkedHashSetTest {
    @Test
    public void add() {
        Set<String> expected = new LinkedHashSet<>();
        expected.add("1");
        expected.add("2");
        expected.add("3");
        String[] actual = new String[]{"1", "2", "3"};
        Assert.assertArrayEquals(actual, expected.toArray());
    }
}
