package com.getjavajob.training.algo08.init.gordienkov.lesson04;

import com.getjavajob.training.algo08.util.StopWatch;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static com.getjavajob.training.algo08.util.ConsoleHelper.println;

public class JdkListsPerformanceTest {
    public static void main(String[] args) {
        addToBeginningArrayList();
        addToBeginningLinkedList();
        addToMiddleArrayList();
        addToMiddleLinkedList();
        addToEndArrayList();
        addToEndLinkedList();
        removeFromBeginningArrayList();
        removeFromBeginningLinkedList();
        removeFromMiddleArrayList();
        removeFromMiddleLinkedList();
        removeFromEndArrayList();
        removeFromEndLinkedList();
    }

    private static void addToBeginningArrayList() {
        long start = StopWatch.start();
        List<String> al = new ArrayList<>();
        for (int i = 0; i < 400000; i++) {
            al.add(0, "a");
        }
        println("ArrayList.add(): " + StopWatch.getElapsedTime(start) + " ms");
    }

    private static void addToBeginningLinkedList() {
        long start = StopWatch.start();
        List<String> ll = new LinkedList<>();
        for (int i = 0; i < 400000; i++) {
            ll.add(0, "a");
        }
        println("LinkedList.add(): " + StopWatch.getElapsedTime(start) + " ms");
    }

    private static void addToMiddleArrayList() {
        long start = StopWatch.start();
        List<String> al = new ArrayList<>();
        for (int i = 0; i < 400000; i++) {
            al.add(al.size() / 2, "a");
        }
        println("ArrayList.add(): " + StopWatch.getElapsedTime(start) + " ms");
    }

    private static void addToMiddleLinkedList() {
        long start = StopWatch.start();
        List<String> ll = new LinkedList<>();
        for (int i = 0; i < 400000; i++) {
            ll.add(ll.size() / 2, "a");
        }
        println("LinkedList.add(): " + StopWatch.getElapsedTime(start) + " ms");
    }

    private static void addToEndArrayList() {
        long start = StopWatch.start();
        List<String> al = new ArrayList<>();
        for (int i = 0; i < 10000000; i++) {
            al.add("a");
        }
        println("ArrayList.add(): " + StopWatch.getElapsedTime(start) + " ms");
    }

    private static void addToEndLinkedList() {
        long start = StopWatch.start();
        List<String> ll = new LinkedList<>();
        for (int i = 0; i < 10000000; i++) {
            ll.add("a");
        }
        println("LinkedList.add(): " + StopWatch.getElapsedTime(start) + " ms");
    }

    private static void removeFromBeginningArrayList() {
        List<String> al = new ArrayList<>();
        for (int i = 0; i < 400000; i++) {
            al.add("a");
        }
        long start = StopWatch.start();
        for (int i = 0; i < 400000; i++) {
            al.remove(0);
        }
        println("ArrayList.remove(): " + StopWatch.getElapsedTime(start) + " ms");
    }

    private static void removeFromBeginningLinkedList() {
        List<String> ll = new LinkedList<>();
        for (int i = 0; i < 400000; i++) {
            ll.add("a");
        }
        long start = StopWatch.start();
        for (int i = 0; i < 400000; i++) {
            ll.remove(0);
        }
        println("LinkedList.remove(): " + StopWatch.getElapsedTime(start) + " ms");
    }

    private static void removeFromMiddleArrayList() {
        List<String> al = new ArrayList<>();
        for (int i = 0; i < 100000; i++) {
            al.add("a");
        }
        long start = StopWatch.start();
        for (int i = 0; i < 100000; i++) {
            al.remove(al.size() / 2);
        }
        println("ArrayList.remove(): " + StopWatch.getElapsedTime(start) + " ms");
    }

    private static void removeFromMiddleLinkedList() {
        List<String> ll = new LinkedList<>();
        for (int i = 0; i < 100000; i++) {
            ll.add("a");
        }
        long start = StopWatch.start();
        for (int i = 0; i < 100000; i++) {
            ll.remove(ll.size() / 2);
        }
        println("LinkedList.remove(): " + StopWatch.getElapsedTime(start) + " ms");
    }

    private static void removeFromEndArrayList() {
        List<String> al = new ArrayList<>();
        for (int i = 0; i < 100000000; i++) {
            al.add("a");
        }
        long start = StopWatch.start();
        for (int i = 0; i < 100000000; i++) {
            al.remove(al.size() - 1);
        }
        println("ArrayList.remove(): " + StopWatch.getElapsedTime(start) + " ms");
    }

    private static void removeFromEndLinkedList() {
        List<String> ll = new LinkedList<>();
        for (int i = 0; i < 100000000; i++) {
            ll.add("a");
        }
        long start = StopWatch.start();
        for (int i = 0; i < 100000000; i++) {
            ll.remove(ll.size() - 1);
        }
        println("LinkedList.remove(): " + StopWatch.getElapsedTime(start) + " ms");
    }
}
