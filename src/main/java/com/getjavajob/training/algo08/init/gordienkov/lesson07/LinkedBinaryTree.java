package com.getjavajob.training.algo08.init.gordienkov.lesson07;

import java.util.*;

/**
 * Concrete implementation of a binary tree using a node-based, linked structure
 *
 * @param <E> element
 */
public class LinkedBinaryTree<E> extends AbstractBinaryTree<E> {
    protected NodeImpl<E> root;
    protected int size;

    // nonpublic utility

    /**
     * Validates the node is an instance of supported {@link NodeImpl} type and casts to it
     *
     * @param n node
     * @return casted {@link NodeImpl} node
     * @throws IllegalArgumentException
     */
    protected NodeImpl<E> validate(Node<E> n) throws IllegalArgumentException {
        if (n == null) {
            return null;
        }
        if (n instanceof NodeImpl) {
            return (NodeImpl<E>) n;
        } else {
            throw new IllegalArgumentException();
        }
    }

    // update methods supported by this class

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        if (size != 0) {
            throw new IllegalStateException("Root Node already exists");
        } else {
            root = new NodeImpl<>(e, null, null, null);
            size++;
            return root;
        }
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        if (validate(n).getLeft() != null && validate(n).getRight() != null) {
            throw new IllegalArgumentException("Node have two children");
        }
        NodeImpl<E> child = new NodeImpl<>(e, validate(n), null, null);
        if (validate(n).getLeft() != null) {
            validate(n).setRight(child);
            size++;
        } else {
            validate(n).setLeft(child);
            size++;
        }
        return child;
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> child = new NodeImpl<>(e, validate(n), null, null);
        if (validate(n).getLeft() != null) {
            throw new IllegalArgumentException("Left Node already exists");
        } else {
            validate(n).left = child;
            size++;
        }
        return child;
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> child = new NodeImpl<>(e, validate(n), null, null);
        if (validate(n).getRight() != null) {
            throw new IllegalArgumentException("Right Node already exists");
        } else {
            validate(n).right = child;
            size++;
        }
        return child;
    }

    /**
     * Replaces the element at {@link Node} <i>n</i> with <i>e</i>
     *
     * @param n node
     * @param e element
     * @return replace element
     * @throws IllegalArgumentException
     */
    @Override
    public E set(Node<E> n, E e) throws IllegalArgumentException {
        E element = validate(n).getElement();
        validate(n).setElement(e);
        return element;
    }

    /**
     * Replaces the element at {@link Node} <i>n</i> with <i>e</i>
     *
     * @param n node
     * @return replace element
     * @throws IllegalArgumentException
     */
    @Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        if (validate(n).getLeft() != null && validate(n).getRight() != null) {
            throw new IllegalArgumentException("Node have two children");
        }
        E element = validate(n).getElement();
        if (validate(n).getLeft() == null || validate(n).getRight() == null) {
            if (validate(n).getLeft() == null && validate(n).getRight() == null) {
                validate(validate(n).getParent()).setParent(null);
            } else if (validate(n).getLeft() != null) {
                if (validate(validate(n).getParent()).getLeft().equals(validate(n))) {
                    validate(validate(n).getParent()).setLeft(validate(n).getLeft());
                } else {
                    validate(validate(n).getParent()).setRight(validate(n).getLeft());
                }
            } else {
                if (validate(validate(n).getParent()).getLeft().equals(validate(n))) {
                    validate(validate(n).getParent()).setLeft(validate(n).getRight());
                } else {
                    validate(validate(n).getParent()).setRight(validate(n).getRight());
                }
            }
        }
        size--;
        return element;
    }

    @Override
    public Node<E> sibling(Node<E> n) throws IllegalArgumentException {
        if (validate(n).equals(root)) {
            return null;
        }
        if (validate(n).parent.getLeft().equals(validate(n))) {
            return validate(n).parent.getRight();
        } else {
            return validate(n).getLeft();
        }
    }

    @Override
    public boolean isInternal(Node<E> n) throws IllegalArgumentException {
        if (validate(n).getLeft() != null && validate(n).getRight() != null) {
            return false;
        } else {
            return validate(n).getLeft() != null || validate(n).getRight() != null;
        }
    }

    @Override
    public Collection<Node<E>> children(Node<E> n) throws IllegalArgumentException {
        Collection<Node<E>> collection = new ArrayList<>();
        if (validate(n).getLeft() == null && validate(n).getRight() == null) {
            return collection;
        }
        if (validate(n).getLeft() != null) {
            collection.add(validate(n).getLeft());
        }
        if (validate(n).getRight() != null) {
            collection.add(validate(n).getRight());
        }
        return collection;
    }

    @Override
    public boolean isExternal(Node<E> n) throws IllegalArgumentException {
        return validate(n).getLeft() == null && validate(n).getRight() == null;
    }

    @Override
    public int childrenNumber(Node<E> n) throws IllegalArgumentException {
        if (validate(n).getLeft() != null && validate(n).getRight() != null) {
            return 2;
        } else if (validate(n).getLeft() != null || validate(n).getRight() != null) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public Collection<Node<E>> inOrder() {
        if (size == 0) {
            return new ArrayList<>();
        } else {
            return inOrder(root, new ArrayList<Node<E>>());
        }
    }

    private Collection<Node<E>> inOrder(NodeImpl<E> node, Collection<Node<E>> collection) {
        if (node == null) {
            return collection;
        } else {
            inOrder(node.left, collection);
            collection.add(node);
            inOrder(node.right, collection);
        }
        return collection;
    }

    @Override
    public Collection<Node<E>> preOrder() {
        if (size == 0) {
            return new ArrayList<>();
        } else {
            return preOrder(root, new ArrayList<Node<E>>());
        }
    }

    private Collection<Node<E>> preOrder(NodeImpl<E> node, Collection<Node<E>> collection) {
        if (node == null) {
            return collection;
        } else {
            collection.add(node);
            preOrder(node.left, collection);
            preOrder(node.right, collection);
        }
        return collection;
    }

    @Override
    public Collection<Node<E>> postOrder() {
        if (size == 0) {
            return new ArrayList<>();
        } else {
            return postOrder(root, new ArrayList<Node<E>>());
        }
    }

    private Collection<Node<E>> postOrder(NodeImpl<E> node, Collection<Node<E>> collection) {
        if (node == null) {
            return collection;
        } else {
            postOrder(node.left, collection);
            postOrder(node.right, collection);
            collection.add(node);
        }
        return collection;
    }

    @Override
    public Collection<Node<E>> breadthFirst() {
        Collection<Node<E>> collection = new ArrayList<>();
        if (root == null) {
            return collection;
        }
        Stack<Node<E>> stack = new Stack<>();
        stack.push(root);
        while (!stack.empty()) {
            Node<E> n = stack.pop();
            collection.add(n);

            if (validate(n).right != null) {
                stack.push(validate(n).right);
            }
            if (validate(n).left != null) {
                stack.push(validate(n).left);
            }
        }
        return collection;
    }

    // {@link Tree} and {@link BinaryTree} implementations

    @Override
    public Node<E> left(Node<E> p) throws IllegalArgumentException {
        return validate(p).getLeft();
    }

    @Override
    public Node<E> right(Node<E> p) throws IllegalArgumentException {
        return validate(p).getRight();
    }

    @Override
    public Node<E> root() {
        return root;
    }

    @Override
    public Node<E> parent(Node<E> n) throws IllegalArgumentException {
        return validate(n).getParent();
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<E> iterator() {
        return new Itr();
    }

    @Override
    public Collection<Node<E>> nodes() {
        return postOrder();
    }

    protected static class NodeImpl<E> implements Node<E> {
        public E element;
        public NodeImpl<E> parent;
        public NodeImpl<E> left;
        public NodeImpl<E> right;

        protected NodeImpl(E element, Node<E> parent, Node<E> left, Node<E> right) {
            this.element = element;
            this.parent = (NodeImpl<E>) parent;
            this.left = (NodeImpl<E>) left;
            this.right = (NodeImpl<E>) right;
        }

        @Override
        public E getElement() {
            return element;
        }

        public void setElement(E element) {
            this.element = element;
        }

        public NodeImpl<E> getParent() {
            return parent;
        }

        public void setParent(NodeImpl<E> parent) {
            this.parent = parent;
        }

        public NodeImpl<E> getLeft() {
            return left;
        }

        public void setLeft(NodeImpl<E> left) {
            this.left = left;
        }

        public NodeImpl<E> getRight() {
            return right;
        }

        public void setRight(NodeImpl<E> right) {
            this.right = right;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof NodeImpl)) return false;
            NodeImpl<?> node = (NodeImpl<?>) o;
            return Objects.equals(getElement(), node.getElement()) &&
                    Objects.equals(getParent(), node.getParent()) &&
                    Objects.equals(getLeft(), node.getLeft()) &&
                    Objects.equals(getRight(), node.getRight());
        }

        @Override
        public int hashCode() {
            return Objects.hash(getElement(), getParent(), getLeft(), getRight());
        }

        @Override
        public String toString() {
            if (element == null) {
                return null;
            } else {
                return element.toString();
            }
        }
    }

    private class Itr implements Iterator<E> {
        private Iterator<Node<E>> it = nodes().iterator();

        @Override
        public boolean hasNext() {
            return it.hasNext();
        }

        @Override
        public E next() {
            return it.next().getElement();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
