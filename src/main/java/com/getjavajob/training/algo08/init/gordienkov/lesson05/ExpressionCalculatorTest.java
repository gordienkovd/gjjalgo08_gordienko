package com.getjavajob.training.algo08.init.gordienkov.lesson05;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExpressionCalculatorTest {
    @Test
    public void getPrecedence() {
        ExpressionCalculator calculator = new ExpressionCalculator();
        assertEquals(1, calculator.getPrecedence('-'));
        assertEquals(1, calculator.getPrecedence('+'));
        assertEquals(2, calculator.getPrecedence('*'));
        assertEquals(2, calculator.getPrecedence('/'));
        assertEquals(0, calculator.getPrecedence('f'));
    }

    @Test
    public void equalsOperators() {
        ExpressionCalculator calculator = new ExpressionCalculator();
        assertTrue(calculator.equalsOperators('+', '-'));
        assertTrue(calculator.equalsOperators('*', '+'));
        assertFalse(calculator.equalsOperators('-', '/'));
    }

    @Test
    public void isOperator() {
        ExpressionCalculator calculator = new ExpressionCalculator();
        assertTrue(calculator.isOperator('+'));
        assertTrue(calculator.isOperator('-'));
        assertTrue(calculator.isOperator('*'));
        assertTrue(calculator.isOperator('/'));
        assertFalse(calculator.isOperator('1'));
    }

    @Test
    public void isOperand() {
        ExpressionCalculator calculator = new ExpressionCalculator();
        assertTrue(calculator.isOperand('0'));
        assertTrue(calculator.isOperand('1'));
        assertTrue(calculator.isOperand('2'));
        assertTrue(calculator.isOperand('3'));
        assertTrue(calculator.isOperand('4'));
        assertTrue(calculator.isOperand('5'));
        assertTrue(calculator.isOperand('6'));
        assertTrue(calculator.isOperand('7'));
        assertTrue(calculator.isOperand('8'));
        assertTrue(calculator.isOperand('9'));
        assertFalse(calculator.isOperand('-'));
    }

    @Test
    public void infixToPostfix() {
        ExpressionCalculator calculator = new ExpressionCalculator();
        String expected = "22+2*22*-22/+";
        assertEquals(expected, calculator.infixToPostfix("(2+2)*2-(2*2)+(2/2)"));
    }

    @Test
    public void calculatePostfix() {
        ExpressionCalculator calculator = new ExpressionCalculator();
        assertEquals(5, calculator.calculatePostfix("22+2*22*-22/+"));
    }

}