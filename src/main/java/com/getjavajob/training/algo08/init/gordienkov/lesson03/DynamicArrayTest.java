package com.getjavajob.training.algo08.init.gordienkov.lesson03;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

import static org.junit.Assert.*;

public class DynamicArrayTest {
    @org.junit.Test
    public void add() throws Exception {
        DynamicArray actual = new DynamicArray();
        List<Object> expected = new ArrayList<>();
        actual.add("test");
        expected.add("test");
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void addToBeginning() throws Exception {
        DynamicArray actual = new DynamicArray();
        List<Object> expected = new ArrayList<>();
        actual.add(0, "test");
        expected.add(0, "test");
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void addToMiddle() throws Exception {
        DynamicArray actual = new DynamicArray();
        List<Object> expected = new ArrayList<>();
        actual.add("test");
        actual.add("test1");
        actual.add("test2");
        expected.add("test");
        expected.add("test1");
        expected.add("test2");
        actual.add(1, "middle");
        expected.add(1, "middle");
        assertArrayEquals(expected.toArray(), actual.toArray());
    }
    @Test
    public void addToEnd() throws Exception {
        DynamicArray actual = new DynamicArray();
        List<Object> expected = new ArrayList<>();
        actual.add("test");
        actual.add("test1");
        actual.add("test2");
        expected.add("test");
        expected.add("test1");
        expected.add("test2");
        actual.add(actual.size() - 1, "end");
        expected.add(expected.size() - 1, "end");
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void set() throws Exception {
        DynamicArray actual = new DynamicArray();
        List<Object> expected = new ArrayList<>();
        actual.add("test");
        actual.add("test1");
        actual.add("test2");
        expected.add("test");
        expected.add("test1");
        expected.add("test2");
        actual.set(1, "set");
        expected.set(1, "set");
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void get() throws Exception {
        DynamicArray actual = new DynamicArray();
        List<Object> expected = new ArrayList<>();
        actual.add("test");
        actual.add("test1");
        actual.add("test2");
        expected.add("test");
        expected.add("test1");
        expected.add("test2");
        Assert.assertEquals(actual.get(1), expected.get(1));
    }

    @Test
    public void remove() throws Exception {
        DynamicArray actual = new DynamicArray();
        List<Object> expected = new ArrayList<>();
        actual.add("test");
        actual.add("test1");
        actual.add("test2");
        expected.add("test");
        expected.add("test1");
        expected.add("test2");
        actual.remove("test1");
        expected.remove("test1");
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void removeFromBeginning() throws Exception {
        DynamicArray actual = new DynamicArray();
        List<Object> expected = new ArrayList<>();
        actual.add("test");
        actual.add("test1");
        actual.add("test2");
        expected.add("test");
        expected.add("test1");
        expected.add("test2");
        actual.remove(0);
        expected.remove(0);
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void removeFromMiddle() throws Exception {
        DynamicArray actual = new DynamicArray();
        List<Object> expected = new ArrayList<>();
        actual.add("test");
        actual.add("test1");
        actual.add("test2");
        expected.add("test");
        expected.add("test1");
        expected.add("test2");
        actual.remove(1);
        expected.remove(1);
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void removeFromEnd() throws Exception {
        DynamicArray actual = new DynamicArray();
        List<Object> expected = new ArrayList<>();
        actual.add("test");
        actual.add("test1");
        actual.add("test2");
        expected.add("test");
        expected.add("test1");
        expected.add("test2");
        actual.remove(actual.size() - 1);
        expected.remove(expected.size() - 1);
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void size() throws Exception {
        DynamicArray actual = new DynamicArray();
        actual.add("test");
        actual.add("test1");
        actual.add("test2");
        assertEquals(3, actual.size());
    }

    @Test
    public void indexOf() throws Exception {
        DynamicArray actual = new DynamicArray();
        actual.add("test");
        actual.add("test1");
        actual.add("test2");
        assertEquals(2, actual.indexOf("test2"));
    }

    @Test
    public void containsTrue() throws Exception {
        DynamicArray actual = new DynamicArray();
        actual.add("test");
        actual.add("test1");
        actual.add("test2");
        assertTrue(actual.contains("test1"));
    }

    @Test
    public void containsFalse() throws Exception {
        DynamicArray actual = new DynamicArray();
        actual.add("test");
        actual.add("test1");
        actual.add("test2");
        assertFalse(actual.contains("test10"));
    }

    @Test
    public void toArray() throws Exception {
        Object[] expected = new Object[]{0, 1, 2};
        DynamicArray actual = new DynamicArray();
        actual.add(0);
        actual.add(1);
        actual.add(2);
        assertArrayEquals(expected, actual.toArray());
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void addException() {
        DynamicArray actual = new DynamicArray();
        actual.add(0);
        actual.add(1);
        actual.add(2);
        actual.add(20, "test");
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void setException() {
        DynamicArray actual = new DynamicArray();
        actual.add(0);
        actual.add(1);
        actual.add(2);
        actual.set(20, "test");
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void getException() {
        DynamicArray actual = new DynamicArray();
        actual.add(0);
        actual.add(1);
        actual.add(2);
        actual.get(20);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void removeException() {
        DynamicArray actual = new DynamicArray();
        actual.add(0);
        actual.add(1);
        actual.add(2);
        actual.remove(5);
    }

    @Test
    public void iteratorHasNextTrue() {
        DynamicArray actual = new DynamicArray();
        actual.add(0);
        actual.add(1);
        actual.add(2);
        DynamicArray.ListIterator iterator = actual.listIterator();
        assertTrue(iterator.hasNext());
    }

    @Test
    public void iteratorHasNextFalse() {
        DynamicArray actual = new DynamicArray();
        DynamicArray.ListIterator iterator = actual.listIterator();
        assertFalse(iterator.hasNext());
    }

    @Test
    public void iteratorNext() {
        DynamicArray actual = new DynamicArray();
        actual.add(0);
        actual.add(1);
        actual.add(2);
        DynamicArray.ListIterator iterator = actual.listIterator();
        assertEquals(0, iterator.next());
    }

    @Test
    public void iteratorHasPreviousTrue() {
        DynamicArray actual = new DynamicArray();
        actual.add(0);
        actual.add(1);
        actual.add(2);
        DynamicArray.ListIterator iterator = actual.listIterator();
        iterator.next();
        assertTrue(iterator.hasPrevious());
    }

    @Test
    public void iteratorHasPreviousFalse() {
        DynamicArray actual = new DynamicArray();
        actual.add(0);
        actual.add(1);
        actual.add(2);
        DynamicArray.ListIterator iterator = actual.listIterator();
        assertFalse(iterator.hasPrevious());
    }

    @Test
    public void iteratorPrevious() {
        DynamicArray actual = new DynamicArray();
        actual.add(0);
        actual.add(1);
        actual.add(2);
        DynamicArray.ListIterator iterator = actual.listIterator();
        iterator.next();
        assertEquals(0, iterator.previous());
    }

    @Test
    public void iteratorNextIndex() {
        DynamicArray actual = new DynamicArray();
        actual.add(0);
        actual.add(1);
        actual.add(2);
        DynamicArray.ListIterator iterator = actual.listIterator();
        assertEquals(0, iterator.nextIndex());
    }

    @Test
    public void iteratorPreviousIndex() {
        DynamicArray actual = new DynamicArray();
        actual.add(0);
        actual.add(1);
        actual.add(2);
        DynamicArray.ListIterator iterator = actual.listIterator();
        iterator.next();
        assertEquals(0, iterator.previousIndex());
    }

    @Test
    public void iteratorRemove() {
        DynamicArray actual = new DynamicArray();
        actual.add(0);
        actual.add(1);
        actual.add(2);
        DynamicArray.ListIterator iterator = actual.listIterator();
        iterator.next();
        iterator.remove();
        assertEquals(1, actual.get(0));
    }

    @Test
    public void iteratorSet() {
        DynamicArray actual = new DynamicArray();
        actual.add(0);
        actual.add(1);
        actual.add(2);
        DynamicArray.ListIterator iterator = actual.listIterator();
        iterator.next();
        iterator.set("test");
        assertEquals("test", actual.get(0));
    }

    @Test
    public void iteratorAdd() {
        DynamicArray actual = new DynamicArray();
        actual.add(0);
        actual.add(1);
        actual.add(2);
        DynamicArray.ListIterator iterator = actual.listIterator();
        iterator.add("test");
        assertEquals("test", actual.get(0));
    }

    @Test(expected = ConcurrentModificationException.class)
    public void iteratorNextException() {
        DynamicArray actual = new DynamicArray();
        actual.add(0);
        actual.add(1);
        actual.add(2);
        DynamicArray.ListIterator iterator = actual.listIterator();
        actual.add(3);
        iterator.next();
    }

    @Test(expected = ConcurrentModificationException.class)
    public void iteratorPreviousException() {
        DynamicArray actual = new DynamicArray();
        actual.add(0);
        actual.add(1);
        actual.add(2);
        DynamicArray.ListIterator iterator = actual.listIterator();
        actual.add(3);
        iterator.previous();
    }

    @Test(expected = ConcurrentModificationException.class)
    public void iteratorRemoveException() {
        DynamicArray actual = new DynamicArray();
        actual.add(0);
        actual.add(1);
        actual.add(2);
        DynamicArray.ListIterator iterator = actual.listIterator();
        iterator.next();
        actual.add(3);
        iterator.remove();
    }

    @Test(expected = ConcurrentModificationException.class)
    public void iteratorSetException() {
        DynamicArray actual = new DynamicArray();
        actual.add(0);
        actual.add(1);
        actual.add(2);
        DynamicArray.ListIterator iterator = actual.listIterator();
        iterator.next();
        actual.add(3);
        iterator.set("test");
    }

    @Test(expected = ConcurrentModificationException.class)
    public void iteratorAddException() {
        DynamicArray actual = new DynamicArray();
        actual.add(0);
        actual.add(1);
        actual.add(2);
        DynamicArray.ListIterator iterator = actual.listIterator();
        actual.add(3);
        iterator.add("test");
    }
}