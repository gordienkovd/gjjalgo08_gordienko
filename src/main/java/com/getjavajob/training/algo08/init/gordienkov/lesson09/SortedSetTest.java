package com.getjavajob.training.algo08.init.gordienkov.lesson09;

import org.junit.Test;

import java.util.NoSuchElementException;
import java.util.SortedSet;
import java.util.TreeSet;

import static org.junit.Assert.assertEquals;

public class SortedSetTest {
    @Test
    public void comparator() {
        SortedSet<Integer> treeSet = new TreeSet<>();
        assertEquals(null, treeSet.comparator());
    }

    @Test
    public void subSet() {
        SortedSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(1);
        treeSet.add(2);
        treeSet.add(3);
        treeSet.add(4);
        treeSet.add(5);
        assertEquals("[1, 2, 3, 4]", treeSet.subSet(1, 5).toString());
    }

    @Test
    public void headSet() {
        SortedSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(1);
        treeSet.add(2);
        treeSet.add(3);
        treeSet.add(4);
        treeSet.add(5);
        assertEquals("[1, 2]", treeSet.headSet(3).toString());
    }

    @Test
    public void tailSet() {
        SortedSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(1);
        treeSet.add(2);
        treeSet.add(3);
        treeSet.add(4);
        treeSet.add(5);
        assertEquals("[3, 4, 5]", treeSet.tailSet(3).toString());
    }

    @Test
    public void first() {
        SortedSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(1);
        treeSet.add(2);
        treeSet.add(3);
        treeSet.add(4);
        treeSet.add(5);
        assertEquals("1", treeSet.first().toString());
    }

    @Test
    public void last() {
        SortedSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(1);
        treeSet.add(2);
        treeSet.add(3);
        treeSet.add(4);
        treeSet.add(5);
        assertEquals("5", treeSet.last().toString());
    }

    @Test(expected = NullPointerException.class)
    public void subSetNullPointerException() {
        SortedSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(1);
        treeSet.add(2);
        treeSet.add(3);
        treeSet.add(4);
        treeSet.add(5);
        treeSet.subSet(null, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void subSetIllegalArgumentException() {
        SortedSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(1);
        treeSet.add(2);
        treeSet.add(3);
        treeSet.add(4);
        treeSet.add(5);
        treeSet.subSet(5, 1);
    }

    @Test(expected = NullPointerException.class)
    public void headSetNullPointerException() {
        SortedSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(1);
        treeSet.add(2);
        treeSet.add(3);
        treeSet.add(4);
        treeSet.add(5);
        treeSet.headSet(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void headIllegalArgumentException() {
        SortedSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(1);
        treeSet.add(2);
        treeSet.add(3);
        treeSet.add(4);
        treeSet.add(5);
        treeSet.subSet(1, 3).headSet(4);
    }

    @Test(expected = NullPointerException.class)
    public void tailSetNullPointerException() {
        SortedSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(1);
        treeSet.add(2);
        treeSet.add(3);
        treeSet.add(4);
        treeSet.add(5);
        treeSet.tailSet(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void tailSetIllegalArgumentException() {
        SortedSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(1);
        treeSet.add(2);
        treeSet.add(3);
        treeSet.add(4);
        treeSet.add(5);
        treeSet.subSet(1, 3).tailSet(4);
    }

    @Test(expected = NoSuchElementException.class)
    public void firstNoSuchElementException() {
        SortedSet<Integer> treeSet = new TreeSet<>();
        treeSet.first();
    }

    @Test(expected = NoSuchElementException.class)
    public void lastNoSuchElementException() {
        SortedSet<Integer> treeSet = new TreeSet<>();
        treeSet.last();
    }
}
