package com.getjavajob.training.algo08.init.gordienkov.lesson06;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MapTest {
    @Test
    public void size() {
        Map<Integer, Integer> expected = new HashMap<>();
        expected.put(1, 1);
        assertEquals(1, expected.size());
    }

    @Test
    public void isEmpty() {
        Map<Integer, Integer> expected = new HashMap<>();
        assertTrue(expected.isEmpty());
        expected.put(1, 1);
        assertFalse(expected.isEmpty());
    }

    @Test
    public void containsKey() {
        Map<Integer, Integer> expected = new HashMap<>();
        assertFalse(expected.containsKey(1));
        expected.put(1, 1);
        assertTrue(expected.containsKey(1));
    }

    @Test
    public void containsValue() {
        Map<Integer, Integer> expected = new HashMap<>();
        assertFalse(expected.containsValue(1));
        expected.put(1, 1);
        assertTrue(expected.containsValue(1));
    }

    @Test
    public void get() {
        Map<String, String> expected = new HashMap<>();
        expected.put("1", "1");
        assertEquals("1", expected.get("1"));
    }

    @Test
    public void put() {
        Map<String, String> expected = new HashMap<>();
        expected.put("1", "1");
        assertEquals("1", expected.get("1"));
    }

    @Test
    public void remove() {
        Map<String, String> expected = new HashMap<>();
        expected.put("1", "1");
        assertEquals("1", expected.remove("1"));
    }

    @Test
    public void putAll() {
        Map<String, String> expected = new HashMap<>();
        Map<String, String> actual = new HashMap<>();
        actual.put("1", "1");
        expected.putAll(actual);
        assertEquals("1", expected.get("1"));
    }

    @Test
    public void clear() {
        Map<String, String> expected = new HashMap<>();
        expected.put("1", "1");
        expected.put("2", "2");
        expected.clear();
        assertTrue(expected.isEmpty());
    }
}
