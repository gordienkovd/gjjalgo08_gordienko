package com.getjavajob.training.algo08.init.gordienkov.lesson05;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import static org.junit.Assert.assertArrayEquals;

public class CollectionUtilsTest {
    @Test
    public void filter() {
        Collection<Worker> actual = new ArrayList<>();
        actual.add(new Worker("Ivan", "Ivanov"));
        actual.add(new Worker("Petr", "Petrov"));
        Collection<Worker> expected = new ArrayList<>();
        expected.add(new Worker("Ivan", "Ivanov"));
        CollectionUtils.filter(actual, new Predicate<Worker>() {
            @Override
            public boolean test(Worker o) {
                return o.getName().equals("Ivan");
            }
        });
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void transformToNewCollection() {
        Collection<Worker> actual = new ArrayList<>();
        actual.add(new Worker("Ivan", "Ivanov"));
        actual.add(new Worker("Petr", "Petrov"));
        Collection<String> expected = new ArrayList<>();
        expected.add("Ivan");
        expected.add("Petr");
        Collection actual2 = CollectionUtils.transformToNewCollection(actual, new Transformer<Worker, String>() {
            @Override
            public String transformCollection(Worker input) {
                return input.getName();
            }
        });
        assertArrayEquals(expected.toArray(), actual2.toArray());
    }

    @Test
    public void transform() throws Exception {
        Collection<Object> actual = new ArrayList<>();
        actual.add(new Worker("Ivan", "Ivanov"));
        actual.add(new Worker("Petr", "Petrov"));
        Collection<Object> expected = new ArrayList<>();
        expected.add("Ivan");
        expected.add("Petr");
        CollectionUtils.transform(actual, new Transformer<Worker, String>() {
            @Override
            public String transformCollection(Worker input) {
                return input.getName();
            }
        });
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void forAllDo() throws Exception {
        Collection<Worker> actual = new ArrayList<>();
        actual.add(new Worker("Ivan", "Ivanov"));
        actual.add(new Worker("Petr", "Petrov"));
        Collection<Worker> expected = new ArrayList<>();
        expected.add(new Worker("Sidor", "Ivanov"));
        expected.add(new Worker("Sidor", "Petrov"));
        CollectionUtils.forAllDo(actual, new Closure<Worker>() {
            @Override
            public void execute(Worker input) {
                input.setName("Sidor");
            }
        });
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test(expected = UnsupportedOperationException.class)
    public void unmodifiableCollectionAdd() throws Exception {
        Collection<Worker> workers = new ArrayList<>();
        workers.add(new Worker("Ivan", "Ivanov"));
        workers.add(new Worker("Petr", "Petrov"));
        Collection actual = CollectionUtils.unmodifiableCollection(workers);
        actual.add(new Worker("Sidor", "Sidorov"));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void unmodifiableCollectionIterator() throws Exception {
        Collection<Worker> workers = new ArrayList<>();
        workers.add(new Worker("Ivan", "Ivanov"));
        workers.add(new Worker("Petr", "Petrov"));
        Collection actual = CollectionUtils.unmodifiableCollection(workers);
        Iterator itr = actual.iterator();
    }

    @Test(expected = UnsupportedOperationException.class)
    public void unmodifiableCollectionRemove() throws Exception {
        Collection<Worker> workers = new ArrayList<>();
        workers.add(new Worker("Ivan", "Ivanov"));
        workers.add(new Worker("Petr", "Petrov"));
        Collection actual = CollectionUtils.unmodifiableCollection(workers);
        actual.remove(new Worker("Ivan", "Ivanov"));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void unmodifiableCollectionAddAll() throws Exception {
        Collection<Worker> workers = new ArrayList<>();
        workers.add(new Worker("Ivan", "Ivanov"));
        workers.add(new Worker("Petr", "Petrov"));
        Collection actual = CollectionUtils.unmodifiableCollection(workers);
        actual.addAll(workers);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void unmodifiableCollectionClear() throws Exception {
        Collection<Worker> workers = new ArrayList<>();
        workers.add(new Worker("Ivan", "Ivanov"));
        workers.add(new Worker("Petr", "Petrov"));
        Collection actual = CollectionUtils.unmodifiableCollection(workers);
        actual.clear();
    }

    @Test(expected = UnsupportedOperationException.class)
    public void unmodifiableCollectionRemoveAll() throws Exception {
        Collection<Worker> workers = new ArrayList<>();
        workers.add(new Worker("Ivan", "Ivanov"));
        workers.add(new Worker("Petr", "Petrov"));
        Collection actual = CollectionUtils.unmodifiableCollection(workers);
        actual.removeAll(workers);
    }
}