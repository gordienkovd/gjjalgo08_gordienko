package com.getjavajob.training.algo08.init.gordienkov.lesson05;

import java.util.*;

public class CollectionUtils {
    public static <T> void filter(Collection<T> collection, Predicate<T> predicate) {
        if (collection != null && predicate != null) {
            Iterator<T> itr = collection.iterator();
            while (itr.hasNext()) {
                T obj = itr.next();
                if (!predicate.test(obj)) {
                    itr.remove();
                }
            }
        }
    }

    public static <I, O> Collection<O> transformToNewCollection(final Collection<I> collection, Transformer<I, O> transformer) {
        if (collection != null && transformer != null) {
            Collection<I> list = new ArrayList<>();
            list.addAll(collection);
            Collection<O> output = (Collection<O>) collection;
            output.clear();
            for (I element : list) {
                output.add(transformer.transformCollection(element));
            }
            return output;
        }
        return null;
    }

    public static void transform(Collection<Object> collection, Transformer transformer) {
        if (collection != null && transformer != null) {
            Collection<Object> list = new ArrayList<>();
            list.addAll(collection);
            collection.clear();
            for (Object element : list) {
                collection.add(transformer.transformCollection(element));
            }
        }
    }

    public static <T> void forAllDo(Collection<T> collection, Closure<T> closure) {
        if (collection != null) {
            for (T element : collection) {
                closure.execute(element);
            }
        }
    }

    public static <T> UnmodifiableCollection unmodifiableCollection(Collection<T> collection) {
        return new UnmodifiableCollection<T>(collection);
    }

    private static class UnmodifiableCollection<T> implements Collection<T> {
        private Collection<T> collection;

        UnmodifiableCollection(Collection<T> collection) {
            this.collection = collection;
        }

        @Override
        public int size() {
            return collection.size();
        }

        @Override
        public boolean isEmpty() {
            return collection.isEmpty();
        }

        @Override
        public boolean contains(Object o) {
            return collection.contains(o);
        }

        @Override
        public Iterator<T> iterator() {
            throw new UnsupportedOperationException();
        }

        @Override
        public Object[] toArray() {
            return collection.toArray();
        }

        @Override
        public boolean add(Object o) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean remove(Object o) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean addAll(Collection c) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void clear() {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean equals(Object o) {
            return collection.equals(o);
        }

        @Override
        public int hashCode() {
            return collection.hashCode();
        }

        @Override
        public boolean retainAll(Collection c) {
            return collection.retainAll(c);
        }

        @Override
        public boolean removeAll(Collection c) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean containsAll(Collection c) {
            return collection.containsAll(c);
        }

        @Override
        public Object[] toArray(Object[] a) {
            return collection.toArray(a);
        }
    }
}
