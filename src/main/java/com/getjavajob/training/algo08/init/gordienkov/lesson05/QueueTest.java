package com.getjavajob.training.algo08.init.gordienkov.lesson05;

import org.junit.Test;

import java.util.ArrayDeque;
import java.util.Queue;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class QueueTest {
    @Test
    public void add() {
        Queue<Integer> actual = new ArrayDeque<>();
        actual.add(1);
        actual.add(2);
        Integer[] expected = new Integer[]{1, 2};
        assertArrayEquals(expected, actual.toArray());
    }

    @Test
    public void offer() {
        Queue<Integer> actual = new ArrayDeque<>();
        actual.offer(1);
        actual.offer(2);
        Integer[] expected = new Integer[]{1, 2};
        assertArrayEquals(expected, actual.toArray());
    }

    @Test
    public void remove() {
        Queue<Integer> actual = new ArrayDeque<>();
        actual.add(1);
        actual.add(2);
        actual.remove();
        Integer[] expected = new Integer[]{2};
        assertArrayEquals(expected, actual.toArray());
    }

    @Test
    public void poll() {
        Queue<Integer> actual = new ArrayDeque<>();
        actual.add(1);
        actual.add(2);
        actual.poll();
        Integer[] expected = new Integer[]{2};
        assertArrayEquals(expected, actual.toArray());
    }

    @Test
    public void element() {
        Queue<String> actual = new ArrayDeque<>();
        actual.add("a");
        actual.add("b");
        assertEquals("a", actual.element());
    }

    @Test
    public void peek() {
        Queue<String> actual = new ArrayDeque<>();
        actual.add("a");
        actual.add("b");
        assertEquals("a", actual.peek());
    }
}
