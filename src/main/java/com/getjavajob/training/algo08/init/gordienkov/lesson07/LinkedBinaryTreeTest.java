package com.getjavajob.training.algo08.init.gordienkov.lesson07;

import org.junit.Test;

import static org.junit.Assert.*;

public class LinkedBinaryTreeTest {
    @Test
    public void addRoot() throws Exception {
        LinkedBinaryTree<String> binaryTree = new LinkedBinaryTree<>();
        assertEquals("a", binaryTree.addRoot("a").getElement());
    }

    @Test
    public void add() throws Exception {
        LinkedBinaryTree<String> binaryTree = new LinkedBinaryTree<>();
        binaryTree.addRoot("a");
        assertEquals("b", binaryTree.add(binaryTree.root(), "b").getElement());
    }

    @Test
    public void addLeft() throws Exception {
        LinkedBinaryTree<String> binaryTree = new LinkedBinaryTree<>();
        binaryTree.addRoot("a");
        assertEquals("b", binaryTree.addLeft(binaryTree.root(), "b").getElement());
    }

    @Test
    public void addRight() throws Exception {
        LinkedBinaryTree<String> binaryTree = new LinkedBinaryTree<>();
        binaryTree.addRoot("a");
        assertEquals("b", binaryTree.addRight(binaryTree.root(), "b").getElement());
    }

    @Test
    public void set() throws Exception {
        LinkedBinaryTree<String> binaryTree = new LinkedBinaryTree<>();
        binaryTree.addRoot("a");
        Node<String> node = binaryTree.add(binaryTree.root(), "b");
        assertEquals("b", binaryTree.set(node, "c"));
    }

    @Test
    public void remove() throws Exception {
        LinkedBinaryTree<String> binaryTree = new LinkedBinaryTree<>();
        binaryTree.addRoot("a");
        binaryTree.add(binaryTree.root(), "b");
        Node<String> node2 = binaryTree.add(binaryTree.root(), "c");
        binaryTree.add(node2, "d");
        assertEquals("c", binaryTree.remove(node2));
    }

    @Test
    public void left() throws Exception {
        LinkedBinaryTree<String> binaryTree = new LinkedBinaryTree<>();
        binaryTree.addRoot("a");
        binaryTree.addLeft(binaryTree.root(), "b");
        assertEquals("b", binaryTree.left(binaryTree.root()).getElement());
    }

    @Test
    public void right() throws Exception {
        LinkedBinaryTree<String> binaryTree = new LinkedBinaryTree<>();
        binaryTree.addRoot("a");
        binaryTree.addRight(binaryTree.root(), "b");
        assertEquals("b", binaryTree.right(binaryTree.root()).getElement());
    }

    @Test
    public void root() throws Exception {
        LinkedBinaryTree<String> binaryTree = new LinkedBinaryTree<>();
        binaryTree.addRoot("a");
        assertEquals("a", binaryTree.root().getElement());
    }

    @Test
    public void parent() throws Exception {
        LinkedBinaryTree<String> binaryTree = new LinkedBinaryTree<>();
        binaryTree.addRoot("a");
        Node<String> node = binaryTree.add(binaryTree.root(), "b");
        assertEquals("a", binaryTree.parent(node).getElement());
    }

    @Test
    public void size() throws Exception {
        LinkedBinaryTree<String> binaryTree = new LinkedBinaryTree<>();
        binaryTree.addRoot("a");
        binaryTree.addLeft(binaryTree.root(), "b");
        binaryTree.addRight(binaryTree.root(), "c");
        assertEquals(3, binaryTree.size());
    }

    @Test
    public void sibling() throws Exception {
        LinkedBinaryTree<String> binaryTree = new LinkedBinaryTree<>();
        binaryTree.addRoot("a");
        Node<String> node = binaryTree.addLeft(binaryTree.root(), "b");
        binaryTree.addRight(binaryTree.root(), "c");
        assertEquals("c", binaryTree.sibling(node).getElement());
    }

    @Test
    public void isInternal() throws Exception {
        LinkedBinaryTree<String> binaryTree = new LinkedBinaryTree<>();
        binaryTree.addRoot("a");
        binaryTree.addLeft(binaryTree.root(), "b");
        assertTrue(binaryTree.isInternal(binaryTree.root()));
        binaryTree.addRight(binaryTree.root(), "c");
        assertFalse(binaryTree.isInternal(binaryTree.root()));
    }

    @Test
    public void isExternal() throws Exception {
        LinkedBinaryTree<String> binaryTree = new LinkedBinaryTree<>();
        binaryTree.addRoot("a");
        assertTrue(binaryTree.isExternal(binaryTree.root()));
        binaryTree.addLeft(binaryTree.root(), "b");
        assertFalse(binaryTree.isExternal(binaryTree.root()));
        binaryTree.addRight(binaryTree.root(), "c");
        assertFalse(binaryTree.isExternal(binaryTree.root()));
    }

    @Test
    public void children() throws Exception {
        LinkedBinaryTree<String> binaryTree = new LinkedBinaryTree<>();
        binaryTree.addRoot("a");
        assertEquals(null, binaryTree.children(binaryTree.root()));
        binaryTree.addLeft(binaryTree.root(), "b");
        assertEquals("[b]", binaryTree.children(binaryTree.root()).toString());
        binaryTree.addRight(binaryTree.root(), "c");
        assertEquals("[b, c]", binaryTree.children(binaryTree.root()).toString());
    }

    @Test
    public void childrenNumber() throws Exception {
        LinkedBinaryTree<String> binaryTree = new LinkedBinaryTree<>();
        binaryTree.addRoot("a");
        assertEquals(0, binaryTree.childrenNumber(binaryTree.root()));
        binaryTree.addLeft(binaryTree.root(), "b");
        assertEquals(1, binaryTree.childrenNumber(binaryTree.root()));
        binaryTree.addRight(binaryTree.root(), "c");
        assertEquals(2, binaryTree.childrenNumber(binaryTree.root()));
    }

    @Test
    public void inOrder() throws Exception {
        LinkedBinaryTree<String> binaryTree = new LinkedBinaryTree<>();
        binaryTree.addRoot("a");
        binaryTree.addLeft(binaryTree.root(), "b");
        binaryTree.addRight(binaryTree.root(), "c");
        assertEquals("[b, a, c]", binaryTree.inOrder().toString());
    }

    @Test
    public void preOrder() throws Exception {
        LinkedBinaryTree<String> binaryTree = new LinkedBinaryTree<>();
        binaryTree.addRoot("a");
        binaryTree.addLeft(binaryTree.root(), "b");
        binaryTree.addRight(binaryTree.root(), "c");
        assertEquals("[a, b, c]", binaryTree.preOrder().toString());
    }

    @Test
    public void postOrder() throws Exception {
        LinkedBinaryTree<String> binaryTree = new LinkedBinaryTree<>();
        binaryTree.addRoot("a");
        binaryTree.addLeft(binaryTree.root(), "b");
        binaryTree.addRight(binaryTree.root(), "c");
        assertEquals("[b, c, a]", binaryTree.postOrder().toString());
    }

    @Test
    public void breadthFirst() throws Exception {
        LinkedBinaryTree<String> binaryTree = new LinkedBinaryTree<>();
        binaryTree.addRoot("a");
        binaryTree.addLeft(binaryTree.root(), "b");
        binaryTree.addRight(binaryTree.root(), "c");
        assertEquals("[a, b, c]", binaryTree.breadthFirst().toString());
    }

    @Test(expected = IllegalStateException.class)
    public void addRootIllegalStateException() {
        LinkedBinaryTree<String> binaryTree = new LinkedBinaryTree<>();
        binaryTree.addRoot("a");
        binaryTree.addRoot("b");
    }

    @Test(expected = IllegalArgumentException.class)
    public void addIllegalArgumentException() {
        LinkedBinaryTree<String> binaryTree = new LinkedBinaryTree<>();
        binaryTree.addRoot("a");
        binaryTree.add(binaryTree.root(), "b");
        binaryTree.add(binaryTree.root(), "c");
        binaryTree.add(binaryTree.root(), "d");
    }

    @Test(expected = IllegalArgumentException.class)
    public void addLeftIllegalArgumentException() {
        LinkedBinaryTree<String> binaryTree = new LinkedBinaryTree<>();
        binaryTree.addRoot("a");
        binaryTree.addLeft(binaryTree.root(), "b");
        binaryTree.addLeft(binaryTree.root(), "c");
    }

    @Test(expected = IllegalArgumentException.class)
    public void addRightIllegalArgumentException() {
        LinkedBinaryTree<String> binaryTree = new LinkedBinaryTree<>();
        binaryTree.addRoot("a");
        binaryTree.addRight(binaryTree.root(), "b");
        binaryTree.addRight(binaryTree.root(), "c");
    }

    @Test(expected = IllegalArgumentException.class)
    public void removeIllegalArgumentException() {
        LinkedBinaryTree<String> binaryTree = new LinkedBinaryTree<>();
        binaryTree.addRoot("a");
        binaryTree.addRight(binaryTree.root(), "b");
        binaryTree.addLeft(binaryTree.root(), "c");
        binaryTree.remove(binaryTree.root());
    }
}