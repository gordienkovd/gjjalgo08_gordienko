package com.getjavajob.training.algo08.init.gordienkov.lesson09;

import org.junit.Test;

import java.util.NoSuchElementException;
import java.util.SortedMap;
import java.util.TreeMap;

import static org.junit.Assert.assertEquals;

public class SortedMapTest {
    @Test
    public void comparator() {
        SortedMap<Integer, Integer> sortedMap = new TreeMap<>();
        assertEquals(null, sortedMap.comparator());
    }

    @Test
    public void subMap() {
        SortedMap<Integer, Integer> sortedMap = new TreeMap<>();
        sortedMap.put(1, 1);
        sortedMap.put(2, 2);
        sortedMap.put(3, 3);
        sortedMap.put(4, 4);
        sortedMap.put(5, 5);
        assertEquals("{1=1, 2=2, 3=3, 4=4}", sortedMap.subMap(1, 5).toString());
    }

    @Test
    public void headMap() {
        SortedMap<Integer, Integer> sortedMap = new TreeMap<>();
        sortedMap.put(1, 1);
        sortedMap.put(2, 2);
        sortedMap.put(3, 3);
        sortedMap.put(4, 4);
        sortedMap.put(5, 5);
        assertEquals("{1=1, 2=2, 3=3, 4=4}", sortedMap.headMap(5).toString());
    }

    @Test
    public void tailMap() {
        SortedMap<Integer, Integer> sortedMap = new TreeMap<>();
        sortedMap.put(1, 1);
        sortedMap.put(2, 2);
        sortedMap.put(3, 3);
        sortedMap.put(4, 4);
        sortedMap.put(5, 5);
        assertEquals("{1=1, 2=2, 3=3, 4=4, 5=5}", sortedMap.tailMap(1).toString());
    }

    @Test
    public void firstKey() {
        SortedMap<Integer, Integer> sortedMap = new TreeMap<>();
        sortedMap.put(1, 1);
        sortedMap.put(2, 2);
        sortedMap.put(3, 3);
        sortedMap.put(4, 4);
        sortedMap.put(5, 5);
        assertEquals("1", sortedMap.firstKey().toString());
    }

    @Test
    public void lastKey() {
        SortedMap<Integer, Integer> sortedMap = new TreeMap<>();
        sortedMap.put(1, 1);
        sortedMap.put(2, 2);
        sortedMap.put(3, 3);
        sortedMap.put(4, 4);
        sortedMap.put(5, 5);
        assertEquals("5", sortedMap.lastKey().toString());
    }

    @Test
    public void keySet() {
        SortedMap<Integer, Integer> sortedMap = new TreeMap<>();
        sortedMap.put(1, 1);
        sortedMap.put(2, 2);
        sortedMap.put(3, 3);
        sortedMap.put(4, 4);
        sortedMap.put(5, 5);
        assertEquals("[1, 2, 3, 4, 5]", sortedMap.keySet().toString());
    }

    @Test
    public void values() {
        SortedMap<Integer, Integer> sortedMap = new TreeMap<>();
        sortedMap.put(1, 1);
        sortedMap.put(2, 2);
        sortedMap.put(3, 3);
        sortedMap.put(4, 4);
        sortedMap.put(5, 5);
        assertEquals("[1, 2, 3, 4, 5]", sortedMap.values().toString());
    }

    @Test
    public void entrySet() {
        SortedMap<Integer, Integer> sortedMap = new TreeMap<>();
        sortedMap.put(1, 1);
        sortedMap.put(2, 2);
        sortedMap.put(3, 3);
        sortedMap.put(4, 4);
        sortedMap.put(5, 5);
        assertEquals("[1=1, 2=2, 3=3, 4=4, 5=5]", sortedMap.entrySet().toString());
    }

    @Test(expected = NullPointerException.class)
    public void subMapNullPointerException() {
        SortedMap<Integer, Integer> sortedMap = new TreeMap<>();
        sortedMap.subMap(null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void subMapIllegalArgumentException() {
        SortedMap<Integer, Integer> sortedMap = new TreeMap<>();
        sortedMap.put(1, 1);
        sortedMap.put(2, 2);
        sortedMap.put(3, 3);
        sortedMap.put(4, 4);
        sortedMap.put(5, 5);
        sortedMap.subMap(1, 5).subMap(6, 9);
    }

    @Test(expected = NullPointerException.class)
    public void headMapNullPointerException() {
        SortedMap<Integer, Integer> sortedMap = new TreeMap<>();
        sortedMap.headMap(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void headMapIllegalArgumentException() {
        SortedMap<Integer, Integer> sortedMap = new TreeMap<>();
        sortedMap.put(1, 1);
        sortedMap.put(2, 2);
        sortedMap.put(3, 3);
        sortedMap.put(4, 4);
        sortedMap.put(5, 5);
        sortedMap.subMap(1, 5).headMap(6);
    }

    @Test(expected = NullPointerException.class)
    public void tailMapNullPointerException() {
        SortedMap<Integer, Integer> sortedMap = new TreeMap<>();
        sortedMap.tailMap(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void tailMapIllegalArgumentException() {
        SortedMap<Integer, Integer> sortedMap = new TreeMap<>();
        sortedMap.put(1, 1);
        sortedMap.put(2, 2);
        sortedMap.put(3, 3);
        sortedMap.put(4, 4);
        sortedMap.put(5, 5);
        sortedMap.subMap(1, 5).tailMap(6);
    }

    @Test(expected = NoSuchElementException.class)
    public void firstKeyNoSuchElementException() {
        SortedMap<Integer, Integer> sortedMap = new TreeMap<>();
        sortedMap.firstKey();
    }

    @Test(expected = NoSuchElementException.class)
    public void lastKeyKeyNoSuchElementException() {
        SortedMap<Integer, Integer> sortedMap = new TreeMap<>();
        sortedMap.lastKey();
    }
}
