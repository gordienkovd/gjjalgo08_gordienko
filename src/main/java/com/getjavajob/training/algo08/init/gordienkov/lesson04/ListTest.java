package com.getjavajob.training.algo08.init.gordienkov.lesson04;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class ListTest {
    @Test
    public void get() {
        List<String> actual = new ArrayList<>();
        actual.add("a");
        actual.add("b");
        actual.add("c");
        assertEquals("b", actual.get(1));
    }

    @Test
    public void set() {
        List<String> actual = new ArrayList<>();
        actual.add("a");
        actual.add("b");
        actual.add("c");
        actual.set(1, "g");
        assertEquals("g", actual.get(1));
    }

    @Test
    public void add() {
        List<String> actual = new ArrayList<>();
        actual.add("a");
        assertEquals("a", actual.get(0));
    }

    @Test
    public void remove() {
        List<String> actual = new ArrayList<>();
        String[] expected = new String[] {"b"};
        actual.add("a");
        actual.add("b");
        actual.remove(0);
        assertArrayEquals(expected, actual.toArray());
    }

    @Test
    public void indexOf() {
        List<String> actual = new ArrayList<>();
        actual.add("a");
        actual.add("b");
        assertEquals(1, actual.indexOf("b"));
    }

    @Test
    public void lastIndexOf() {
        List<String> actual = new ArrayList<>();
        actual.add("a");
        actual.add("b");
        actual.add("b");
        assertEquals(2, actual.lastIndexOf("b"));
    }

    @Test
    public void subList() {
        List<String> actual = new ArrayList<>();
        String[] expected = new String[] {"b"};
        actual.add("a");
        actual.add("b");
        actual.add("c");
        assertArrayEquals(expected, actual.subList(1, 2).toArray());
    }
}
