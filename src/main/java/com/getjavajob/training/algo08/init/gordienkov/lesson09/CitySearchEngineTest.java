package com.getjavajob.training.algo08.init.gordienkov.lesson09;

import org.junit.Test;

import java.util.Set;
import java.util.TreeSet;

import static com.getjavajob.training.algo08.init.gordienkov.lesson09.CitySearchEngine.fillStorage;
import static com.getjavajob.training.algo08.init.gordienkov.lesson09.CitySearchEngine.searchCities;
import static org.junit.Assert.assertEquals;

public class CitySearchEngineTest {
    @Test
    public void searchCitiesTest() throws Exception {
        Set<String> cityNames = new TreeSet<>();
        cityNames.add("moscow");
        cityNames.add("Mogilev");
        cityNames.add("Lobnya");
        cityNames.add("Tver");
        cityNames.add("Ostashkov");
        cityNames.add("Zapadnaya Dvina");
        assertEquals("[Mogilev, moscow]", searchCities(fillStorage(cityNames), "mo").toString());
    }

    @Test
    public void searchCitiesTest2() throws Exception {
        Set<String> cityNames = new TreeSet<>();
        cityNames.add("moscow");
        cityNames.add("Mogilev");
        cityNames.add("Lobnya");
        cityNames.add("Tver");
        cityNames.add("Ostashkov");
        cityNames.add("Zapadnaya Dvina");
        assertEquals(null, searchCities(fillStorage(cityNames), "\uffff"));
    }

    @Test
    public void fillStorageTest() throws Exception {
        Set<String> cityNames = new TreeSet<>();
        cityNames.add("moscow");
        cityNames.add("Mogilev");
        cityNames.add("Lobnya");
        cityNames.add("Tver");
        cityNames.add("Ostashkov");
        cityNames.add("Zapadnaya Dvina");
        assertEquals("[Lobnya, Mogilev, moscow, Ostashkov, Tver, Zapadnaya Dvina]", fillStorage(cityNames).toString());
    }

    @Test(expected = NullPointerException.class)
    public void searchCitiesNullPointerException() {
        searchCities(null, null);
    }

    @Test(expected = NullPointerException.class)
    public void fillStorageNullPointerException() {
        fillStorage(null);
    }
}