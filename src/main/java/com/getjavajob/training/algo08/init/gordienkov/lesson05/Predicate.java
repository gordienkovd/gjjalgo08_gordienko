package com.getjavajob.training.algo08.init.gordienkov.lesson05;

public interface Predicate<T> {
    boolean test(T o);
}
