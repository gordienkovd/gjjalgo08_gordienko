package com.getjavajob.training.algo08.init.gordienkov.lesson09;

import org.junit.Test;

import java.util.NavigableSet;
import java.util.TreeSet;

import static org.junit.Assert.assertEquals;

public class NavigableSetTest {
    @Test
    public void lower() {
        NavigableSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(1);
        treeSet.add(2);
        treeSet.add(3);
        treeSet.add(4);
        treeSet.add(5);
        assertEquals("1", treeSet.lower(2).toString());
        assertEquals(null, treeSet.lower(1));
    }

    @Test
    public void floor() {
        NavigableSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(1);
        treeSet.add(2);
        treeSet.add(3);
        treeSet.add(4);
        treeSet.add(5);
        assertEquals("3", treeSet.floor(3).toString());
        assertEquals(null, treeSet.floor(0));
    }

    @Test
    public void ceiling() {
        NavigableSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(1);
        treeSet.add(2);
        treeSet.add(3);
        treeSet.add(4);
        treeSet.add(5);
        assertEquals("3", treeSet.ceiling(3).toString());
        assertEquals(null, treeSet.ceiling(6));
    }

    @Test
    public void higher() {
        NavigableSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(1);
        treeSet.add(2);
        treeSet.add(3);
        treeSet.add(4);
        treeSet.add(5);
        assertEquals("5", treeSet.higher(4).toString());
        assertEquals(null, treeSet.higher(5));
    }

    @Test
    public void pollFirst() {
        NavigableSet<Integer> treeSet = new TreeSet<>();
        assertEquals(null, treeSet.pollFirst());
        treeSet.add(1);
        treeSet.add(2);
        treeSet.add(3);
        treeSet.add(4);
        treeSet.add(5);
        assertEquals("1", treeSet.pollFirst().toString());
    }

    @Test
    public void pollLast() {
        NavigableSet<Integer> treeSet = new TreeSet<>();
        assertEquals(null, treeSet.pollLast());
        treeSet.add(1);
        treeSet.add(2);
        treeSet.add(3);
        treeSet.add(4);
        treeSet.add(5);
        assertEquals("5", treeSet.pollLast().toString());
    }

    @Test
    public void descendingSet() {
        NavigableSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(1);
        treeSet.add(2);
        treeSet.add(3);
        treeSet.add(4);
        treeSet.add(5);
        assertEquals("[5, 4, 3, 2, 1]", treeSet.descendingSet().toString());
    }

    @Test
    public void subSet() {
        NavigableSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(1);
        treeSet.add(2);
        treeSet.add(3);
        treeSet.add(4);
        treeSet.add(5);
        assertEquals("[1, 2, 3, 4, 5]", treeSet.subSet(1, true, 5, true).toString());
        assertEquals("[2, 3, 4, 5]", treeSet.subSet(1, false, 5, true).toString());
        assertEquals("[1, 2, 3, 4]", treeSet.subSet(1, true, 5, false).toString());
        assertEquals("[2, 3, 4]", treeSet.subSet(1, false, 5, false).toString());
    }

    @Test
    public void headSet() {
        NavigableSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(1);
        treeSet.add(2);
        treeSet.add(3);
        treeSet.add(4);
        treeSet.add(5);
        assertEquals("[1, 2, 3, 4, 5]", treeSet.headSet(5, true).toString());
        assertEquals("[1, 2, 3, 4]", treeSet.headSet(5, false).toString());
    }

    @Test
    public void tailSet() {
        NavigableSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(1);
        treeSet.add(2);
        treeSet.add(3);
        treeSet.add(4);
        treeSet.add(5);
        assertEquals("[1, 2, 3, 4, 5]", treeSet.tailSet(1, true).toString());
        assertEquals("[2, 3, 4, 5]", treeSet.tailSet(1, false).toString());
    }

    @Test(expected = NullPointerException.class)
    public void lowerNullPointerException() {
        NavigableSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(1);
        treeSet.add(2);
        treeSet.add(3);
        treeSet.add(4);
        treeSet.add(5);
        treeSet.lower(null);
    }

    @Test(expected = NullPointerException.class)
    public void floorNullPointerException() {
        NavigableSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(1);
        treeSet.add(2);
        treeSet.add(3);
        treeSet.add(4);
        treeSet.add(5);
        treeSet.floor(null);
    }

    @Test(expected = NullPointerException.class)
    public void ceilingNullPointerException() {
        NavigableSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(1);
        treeSet.add(2);
        treeSet.add(3);
        treeSet.add(4);
        treeSet.add(5);
        treeSet.ceiling(null);
    }

    @Test(expected = NullPointerException.class)
    public void higherNullPointerException() {
        NavigableSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(1);
        treeSet.add(2);
        treeSet.add(3);
        treeSet.add(4);
        treeSet.add(5);
        treeSet.higher(null);
    }

    @Test(expected = NullPointerException.class)
    public void subSetNullPointerException() {
        NavigableSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(1);
        treeSet.add(2);
        treeSet.add(3);
        treeSet.add(4);
        treeSet.add(5);
        treeSet.subSet(null, true, null, true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void subSetIllegalArgumentException() {
        NavigableSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(1);
        treeSet.add(2);
        treeSet.add(3);
        treeSet.add(4);
        treeSet.add(5);
        treeSet.subSet(1, true, 5, true).subSet(1, true, 50, true);
    }

    @Test(expected = NullPointerException.class)
    public void headSetNullPointerException() {
        NavigableSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(1);
        treeSet.add(2);
        treeSet.add(3);
        treeSet.add(4);
        treeSet.add(5);
        treeSet.headSet(null, true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void headSetIllegalArgumentException() {
        NavigableSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(1);
        treeSet.add(2);
        treeSet.add(3);
        treeSet.add(4);
        treeSet.add(5);
        treeSet.headSet(5, true).headSet(50, true);
    }

    @Test(expected = NullPointerException.class)
    public void tailSetNullPointerException() {
        NavigableSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(1);
        treeSet.add(2);
        treeSet.add(3);
        treeSet.add(4);
        treeSet.add(5);
        treeSet.tailSet(null, true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void tailSetIllegalArgumentException() {
        NavigableSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(1);
        treeSet.add(2);
        treeSet.add(3);
        treeSet.add(4);
        treeSet.add(5);
        treeSet.tailSet(1, true).tailSet(0, true);
    }
}
