package com.getjavajob.training.algo08.init.gordienkov.lesson05;

import java.util.ArrayList;
import java.util.List;

public class LinkedListQueue<E> extends AbstractLinkedListQueue<E> {
    private Node<E> head;

    @Override
    public boolean add(E e) {
        if (head == null) {
            head = new Node<>();
            head.val = e;
        } else if (head.next == null) {
            Node<E> node = new Node<>();
            node.val = e;
            head.next = node;
        } else {
            Node<E> node = head;
            while (node.next != null) {
                node = node.next;
            }
            node.next = new Node<>();
            node.next.val = e;
        }
        return true;
    }

    @Override
    public E remove() {
        if (head == null) {
            return null;
        } else if (head.next == null) {
            E oldValue = head.val;
            head = null;
            return oldValue;
        } else {
            Node<E> node = head;
            head = head.next;
            return node.val;
        }
    }

    public List<E> asList() {
        List<E> list = new ArrayList<>();
        Node<E> node = head;
        while (node != null) {
            list.add(node.val);
            node = node.next;
        }
        return list;
    }

    private static class Node<E> {
        Node<E> next;
        E val;
    }
}
