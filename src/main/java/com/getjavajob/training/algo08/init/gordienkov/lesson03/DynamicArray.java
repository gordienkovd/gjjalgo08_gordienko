package com.getjavajob.training.algo08.init.gordienkov.lesson03;

import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;

import static java.lang.System.arraycopy;
import static java.util.Arrays.copyOf;

public class DynamicArray {
    private static final int DEFAULT_CAPACITY = 10;
    private int size;
    private Object[] data;
    private int modCount;

    public DynamicArray() {
        data = new Object[DEFAULT_CAPACITY];
    }

    public DynamicArray(int capacity) {
        if (capacity < 0) {
            throw new IllegalArgumentException("Illegal Capacity: " + capacity);
        } else {
            data = new Object[capacity];
        }
    }

    public boolean add(Object e) {
        modCount++;
        increaseCapacityForAdd();
        data[size] = e;
        size++;
        return true;
    }

    public void add(int i, Object e) {
        checkRangeAdd(i);
        modCount++;
        increaseCapacityForAdd();
        if (i != size) {
            arraycopy(data, i, data, i + 1, size - i);
        }
        data[i] = e;
        size++;
    }

    public Object set(int i, Object e) {
        checkRange(i);
        modCount++;
        Object oldValue = data[i];
        data[i] = e;
        return oldValue;
    }

    public Object get(int i) {
        checkRange(i);
        return data[i];
    }

    public Object remove(int i) {
        checkRange(i);
        modCount++;
        Object oldValue = data[i];
        if (i == size - 1) {
            data[--size] = null;
        } else {
            fastRemove(i);
        }
        return oldValue;
    }

    public boolean remove(Object o) {
        if (o == null) {
            for (int index = 0; index < size; index++)
                if (data[index] == null) {
                    fastRemove(index);
                    return true;
                }
        } else {
            for (int index = 0; index < size; index++)
                if (o.equals(data[index])) {
                    fastRemove(index);
                    return true;
                }
        }
        return false;
    }

    private void fastRemove(int index) {
        modCount++;
        int numMoved = size - index - 1;
        if (numMoved > 0) {
            arraycopy(data, index + 1, data, index,
                    numMoved);
            data[--size] = null;
        }
    }

    public int size() {
        return size;
    }

    public int indexOf(Object e) {
        if (e == null) {
            for (int i = 0; i < size; i++)
                if (data[i] == null)
                    return i;
        } else {
            for (int i = 0; i < size; i++)
                if (e.equals(data[i]))
                    return i;
        }
        return -1;
    }

    public boolean contains(Object e) {
        return indexOf(e) >= 0;
    }

    public Object[] toArray() {
        return copyOf(data, size);
    }

    private void checkRange(int index) {
        if (index < 0 || index >= size) {
            throw new ArrayIndexOutOfBoundsException("index: " + index + ", size: " + size);
        }
    }

    private void checkRangeAdd(int index) {
        if (index < 0 || index > size) {
            throw new ArrayIndexOutOfBoundsException("index: " + index + ", size: " + size);
        }
    }

    private void increaseCapacity(int capacity) {
        Object[] exprData = new Object[capacity];
        arraycopy(data, 0, exprData, 0, size);
        data = exprData;
    }

    private void increaseCapacityForAdd() {
        if (data.length == size) {
            if (data.length == 0) {
                data = new Object[1];
            } else if (data.length == 1) {
                increaseCapacity(data.length + 1);
            } else {
                increaseCapacity(data.length + data.length / 2);
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append('[');
        for (int i = 0; i < size; i++) {
            if (i == size - 1) {
                stringBuilder.append(data[i]).append(']');
            } else {
                stringBuilder.append(data[i]).append(", ");
            }
        }
        return stringBuilder.toString();
    }

    public ListIterator listIterator() {
        return new ListIterator(0);
    }

    public class ListIterator {
        private int cursor;
        private int lastRet = -1;
        private int expectedModCount = modCount;

        public ListIterator(int index) {
            cursor = index;
        }

        public boolean hasNext() {
            return cursor != size;
        }

        public Object next() {
            checkForComodification();
            int i = cursor;
            if (i >= size) {
                throw new NoSuchElementException();
            }
            Object[] data = DynamicArray.this.data;
            if (i >= data.length) {
                throw new ConcurrentModificationException();
            }
            cursor = i + 1;
            return data[lastRet = i];
        }

        public boolean hasPrevious() {
            return cursor != 0;
        }

        public Object previous() {
            checkForComodification();
            int i = cursor - 1;
            if (i < 0) {
                throw new NoSuchElementException();
            }
            Object[] data = DynamicArray.this.data;
            if (i >= data.length) {
                throw new ConcurrentModificationException();
            }
            cursor = i;
            return data[lastRet = i];
        }

        public int nextIndex() {
            return cursor;
        }

        public int previousIndex() {
            return cursor - 1;
        }

        public void remove() {
            if (lastRet < 0) {
                throw new IllegalStateException();
            }
            checkForComodification();
            try {
                DynamicArray.this.remove(lastRet);
                cursor = lastRet;
                lastRet = -1;
                expectedModCount = modCount;
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }

        public void set(Object e) {
            if (lastRet < 0) {
                throw new IllegalStateException();
            }
            checkForComodification();
            try {
                DynamicArray.this.set(lastRet, e);
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }

        public void add(Object e) {
            checkForComodification();
            try {
                int i = cursor;
                DynamicArray.this.add(i, e);
                cursor = i + 1;
                lastRet = -1;
                expectedModCount = modCount;
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }

        final void checkForComodification() {
            if (modCount != expectedModCount) {
                throw new ConcurrentModificationException();
            }
        }
    }
}
