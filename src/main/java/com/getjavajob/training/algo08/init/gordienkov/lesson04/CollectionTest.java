package com.getjavajob.training.algo08.init.gordienkov.lesson04;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.*;

public class CollectionTest {
    @Test
    public void collectionSize() {
        Collection<Integer> collection = new ArrayList<>();
        assertEquals(0, collection.size());
    }

    @Test
    public void collectionIsEmptyTrue() {
        Collection<Integer> collection = new ArrayList<>();
        assertTrue(collection.isEmpty());
    }

    @Test
    public void collectionIsEmptyFalse() {
        Collection<Integer> collection = new ArrayList<>();
        collection.add(1);
        assertFalse(collection.isEmpty());
    }

    @Test
    public void collectionContainsTrue() {
        Collection<Integer> collection = new ArrayList<>();
        collection.add(1);
        assertTrue(collection.contains(1));
    }

    @Test
    public void collectionContainsFalse() {
        Collection<Integer> collection = new ArrayList<>();
        collection.add(1);
        assertFalse(collection.contains(2));
    }

    @Test
    public void collectionToArray() {
        Collection<Integer> collection = new ArrayList<>();
        collection.add(1);
        collection.add(2);
        Integer[] expected = new Integer[] {1, 2};
        assertArrayEquals(expected, collection.toArray());
    }

    @Test
    public void collectionAdd() {
        Collection<Integer> collection = new ArrayList<>();
        collection.add(1);
        Integer[] expected = new Integer[] {1};
        assertArrayEquals(expected, collection.toArray());
    }

    @Test
    public void collectionRemove() {
        Collection<Integer> collection = new ArrayList<>();
        collection.add(1);
        collection.add(2);
        Integer[] expected = new Integer[] {1};
        collection.remove(2);
        assertArrayEquals(expected, collection.toArray());
    }

    @Test
    public void collectionContainsAllTrue() {
        Collection<Integer> actual = new ArrayList<>();
        actual.add(1);
        actual.add(2);
        Collection<Integer> expected = new ArrayList<>();
        expected.add(1);
        expected.add(2);
        assertTrue(actual.containsAll(expected));
    }

    @Test
    public void collectionContainsAllFalse() {
        Collection<Integer> actual = new ArrayList<>();
        actual.add(1);
        Collection<Integer> expected = new ArrayList<>();
        expected.add(1);
        expected.add(2);
        assertFalse(actual.containsAll(expected));
    }

    @Test
    public void collectionAddAll() {
        Collection<Integer> actual = new ArrayList<>();
        actual.add(1);
        actual.add(2);
        Collection<Integer> actual2 = new ArrayList<>();
        actual2.add(3);
        actual2.add(4);
        Collection<Integer> expected = new ArrayList<>();
        expected.add(1);
        expected.add(2);
        expected.add(3);
        expected.add(4);
        actual.addAll(actual2);
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void collectionRemoveAll() {
        Collection<Integer> actual = new ArrayList<>();
        actual.add(1);
        actual.add(2);
        actual.add(3);
        actual.add(4);
        Collection<Integer> actual2 = new ArrayList<>();
        actual2.add(3);
        actual2.add(4);
        Collection<Integer> expected = new ArrayList<>();
        expected.add(1);
        expected.add(2);
        actual.removeAll(actual2);
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void collectionRetainAll() {
        Collection<Integer> actual = new ArrayList<>();
        actual.add(1);
        actual.add(2);
        actual.add(3);
        actual.add(4);
        Collection<Integer> actual2 = new ArrayList<>();
        actual2.add(3);
        actual2.add(4);
        Collection<Integer> expected = new ArrayList<>();
        expected.add(3);
        expected.add(4);
        actual.retainAll(actual2);
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void collectionClear() {
        Collection<Integer> actual = new ArrayList<>();
        actual.add(1);
        actual.add(2);
        actual.add(3);
        actual.add(4);
        actual.clear();
        Integer[] expected = new Integer[0];
        assertArrayEquals(expected, actual.toArray());
    }
}
