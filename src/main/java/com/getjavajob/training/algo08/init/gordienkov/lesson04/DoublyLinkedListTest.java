package com.getjavajob.training.algo08.init.gordienkov.lesson04;

import org.junit.Test;

import java.util.ConcurrentModificationException;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import static org.junit.Assert.*;

public class DoublyLinkedListTest {
    @Test
    public void add() throws Exception {
        List<String> actual = new DoublyLinkedList<>();
        List<String> expected = new LinkedList<>();
        actual.add("test");
        expected.add("test");
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void addToBeginning() throws Exception {
        List<String> actual = new DoublyLinkedList<>();
        List<String> expected = new LinkedList<>();
        actual.add(0, "test");
        expected.add(0, "test");
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void addToMiddle() throws Exception {
        List<String> actual = new DoublyLinkedList<>();
        List<String> expected = new LinkedList<>();
        actual.add("test1");
        actual.add("test2");
        actual.add("test3");
        expected.add("test1");
        expected.add("test2");
        expected.add("test3");
        actual.add(1, "test");
        expected.add(1, "test");
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void addToEnd() throws Exception {
        List<String> actual = new DoublyLinkedList<>();
        List<String> expected = new LinkedList<>();
        actual.add("test1");
        actual.add("test2");
        actual.add("test3");
        expected.add("test1");
        expected.add("test2");
        expected.add("test3");
        actual.add(actual.size(), "test");
        expected.add(expected.size(), "test");
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void remove() throws Exception {
        List<String> actual = new DoublyLinkedList<>();
        List<String> expected = new LinkedList<>();
        actual.add("test1");
        actual.add("test2");
        actual.add("test3");
        expected.add("test1");
        expected.add("test2");
        expected.add("test3");
        actual.remove("test2");
        expected.remove("test2");
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void removeFromBeginning() throws Exception {
        List<String> actual = new DoublyLinkedList<>();
        List<String> expected = new LinkedList<>();
        actual.add("test1");
        actual.add("test2");
        actual.add("test3");
        expected.add("test1");
        expected.add("test2");
        expected.add("test3");
        actual.remove(0);
        expected.remove(0);
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void removeFromMiddle() throws Exception {
        List<String> actual = new DoublyLinkedList<>();
        List<String> expected = new LinkedList<>();
        actual.add("test1");
        actual.add("test2");
        actual.add("test3");
        expected.add("test1");
        expected.add("test2");
        expected.add("test3");
        actual.remove(actual.size() / 2);
        expected.remove(expected.size() / 2);
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void removeFromEnd() throws Exception {
        List<String> actual = new DoublyLinkedList<>();
        List<String> expected = new LinkedList<>();
        actual.add("test1");
        actual.add("test2");
        actual.add("test3");
        expected.add("test1");
        expected.add("test2");
        expected.add("test3");
        actual.remove(actual.size() - 1);
        expected.remove(expected.size() - 1);
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void get() throws Exception {
        List<String> actual = new DoublyLinkedList<>();
        actual.add("test1");
        actual.add("test2");
        actual.add("test3");
        assertEquals("test2", actual.get(1));
    }

    @Test
    public void size() throws Exception {
        List<String> actual = new DoublyLinkedList<>();
        actual.add("test1");
        actual.add("test2");
        actual.add("test3");
        assertEquals(3, actual.size());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void addException() {
        List<String> actual = new DoublyLinkedList<>();
        actual.add("test1");
        actual.add("test2");
        actual.add("test3");
        actual.add(20, "test");
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeException() {
        List<String> actual = new DoublyLinkedList<>();
        actual.add("test1");
        actual.add("test2");
        actual.add("test3");
        actual.remove(20);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void getException() {
        List<String> actual = new DoublyLinkedList<>();
        actual.add("test1");
        actual.add("test2");
        actual.add("test3");
        actual.get(20);
    }

    @Test
    public void iteratorHasNextTrue() {
        List<String> actual = new DoublyLinkedList<>();
        actual.add("test1");
        actual.add("test2");
        actual.add("test3");
        ListIterator<String> iterator = actual.listIterator();
        assertTrue(iterator.hasNext());
    }

    @Test
    public void iteratorHasNextFalse() {
        List<String> actual = new DoublyLinkedList<>();
        ListIterator<String> iterator = actual.listIterator();
        assertFalse(iterator.hasNext());
    }

    @Test
    public void iteratorNext() {
        List<String> actual = new DoublyLinkedList<>();
        actual.add("test1");
        actual.add("test2");
        actual.add("test3");
        ListIterator<String> iterator = actual.listIterator();
        assertEquals("test1", iterator.next());
    }

    @Test
    public void iteratorHasPreviousTrue() {
        List<String> actual = new DoublyLinkedList<>();
        actual.add("test1");
        actual.add("test2");
        actual.add("test3");
        ListIterator<String> iterator = actual.listIterator();
        iterator.next();
        assertTrue(iterator.hasPrevious());
    }

    @Test
    public void iteratorHasPreviousFalse() {
        List<String> actual = new DoublyLinkedList<>();
        actual.add("test1");
        actual.add("test2");
        actual.add("test3");
        ListIterator<String> iterator = actual.listIterator();
        assertFalse(iterator.hasPrevious());
    }

    @Test
    public void iteratorPrevious() {
        List<String> actual = new DoublyLinkedList<>();
        actual.add("test1");
        actual.add("test2");
        actual.add("test3");
        ListIterator<String> iterator = actual.listIterator();
        iterator.next();
        assertEquals("test1", iterator.previous());
    }

    @Test
    public void iteratorNextIndex() {
        List<String> actual = new DoublyLinkedList<>();
        actual.add("test1");
        actual.add("test2");
        actual.add("test3");
        ListIterator<String> iterator = actual.listIterator();
        assertEquals(0, iterator.nextIndex());
    }

    @Test
    public void iteratorPreviousIndex() {
        List<String> actual = new DoublyLinkedList<>();
        actual.add("test1");
        actual.add("test2");
        actual.add("test3");
        ListIterator<String> iterator = actual.listIterator();
        iterator.next();
        assertEquals(0, iterator.previousIndex());
    }

    @Test
    public void iteratorRemove() {
        List<String> actual = new DoublyLinkedList<>();
        actual.add("test1");
        actual.add("test2");
        actual.add("test3");
        ListIterator<String> iterator = actual.listIterator();
        iterator.next();
        iterator.remove();
        assertEquals("test2", actual.get(0));
    }

    @Test
    public void iteratorSet() {
        List<String> actual = new DoublyLinkedList<>();
        actual.add("test1");
        actual.add("test2");
        actual.add("test3");
        ListIterator<String> iterator = actual.listIterator();
        iterator.next();
        iterator.set("test");
        assertEquals("test", actual.get(0));
    }

    @Test
    public void iteratorAdd() {
        List<String> actual = new DoublyLinkedList<>();
        actual.add("test1");
        actual.add("test2");
        actual.add("test3");
        ListIterator<String> iterator = actual.listIterator();
        iterator.add("test");
        assertEquals("test", actual.get(0));
    }

    @Test(expected = ConcurrentModificationException.class)
    public void iteratorNextException() {
        List<String> actual = new DoublyLinkedList<>();
        actual.add("test1");
        actual.add("test2");
        actual.add("test3");
        ListIterator<String> iterator = actual.listIterator();
        actual.add("test");
        iterator.next();
    }

    @Test(expected = ConcurrentModificationException.class)
    public void iteratorPreviousException() {
        List<String> actual = new DoublyLinkedList<>();
        actual.add("test1");
        actual.add("test2");
        actual.add("test3");
        ListIterator<String> iterator = actual.listIterator();
        actual.add("test");
        iterator.previous();
    }

    @Test(expected = ConcurrentModificationException.class)
    public void iteratorRemoveException() {
        List<String> actual = new DoublyLinkedList<>();
        actual.add("test1");
        actual.add("test2");
        actual.add("test3");
        ListIterator<String> iterator = actual.listIterator();
        iterator.next();
        actual.add("test");
        iterator.remove();
    }

    @Test(expected = ConcurrentModificationException.class)
    public void iteratorSetException() {
        List<String> actual = new DoublyLinkedList<>();
        actual.add("test1");
        actual.add("test2");
        actual.add("test3");
        ListIterator<String> iterator = actual.listIterator();
        iterator.next();
        actual.add("test");
        iterator.set("test");
    }

    @Test(expected = ConcurrentModificationException.class)
    public void iteratorAddException() {
        List<String> actual = new DoublyLinkedList<>();
        actual.add("test1");
        actual.add("test2");
        actual.add("test3");
        ListIterator<String> iterator = actual.listIterator();
        actual.add("test");
        iterator.add("test");
    }
}