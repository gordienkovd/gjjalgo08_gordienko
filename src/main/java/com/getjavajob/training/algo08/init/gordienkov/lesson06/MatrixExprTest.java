package com.getjavajob.training.algo08.init.gordienkov.lesson06;

import static com.getjavajob.training.algo08.util.ConsoleHelper.println;

public class MatrixExprTest {
    public static void main(String[] args) {
        MatrixExpr matrix = new MatrixExpr();
        for (int i = 0; i < 1000000; i++) {
            matrix.set(i, i, i);
        }
        println(matrix.get(999999, 999999));
    }
}