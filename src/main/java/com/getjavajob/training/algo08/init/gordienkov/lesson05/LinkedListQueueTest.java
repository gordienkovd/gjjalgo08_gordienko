package com.getjavajob.training.algo08.init.gordienkov.lesson05;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class LinkedListQueueTest {
    @Test
    public void add() throws Exception {
        LinkedListQueue<Integer> actual = new LinkedListQueue<>();
        actual.add(1);
        actual.add(2);
        actual.add(3);
        Integer[] expected = new Integer[]{1, 2, 3};
        assertArrayEquals(expected, actual.asList().toArray());
    }

    @Test
    public void remove() throws Exception {
        LinkedListQueue<Integer> actual = new LinkedListQueue<>();
        actual.add(1);
        actual.add(2);
        actual.add(3);
        actual.remove();
        Integer[] expected = new Integer[]{2, 3};
        assertArrayEquals(expected, actual.asList().toArray());
    }
}