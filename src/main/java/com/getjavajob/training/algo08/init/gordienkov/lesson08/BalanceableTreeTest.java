package com.getjavajob.training.algo08.init.gordienkov.lesson08;

import com.getjavajob.training.algo08.init.gordienkov.lesson07.Node;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BalanceableTreeTest {
    @Test
    public void rotate() throws Exception {
        BalanceableTree<Integer> btree = new BalanceableTree<Integer>() {
        };
        Node<Integer> root = btree.addRoot(0);
        Node<Integer> left = btree.addLeft(root, 1);
        btree.addRight(root, 2);
        btree.rotate(left);
        assertEquals((Integer) 1, btree.root().getElement());
    }

    @Test
    public void reduceSubtreeHeight() throws Exception {
        BalanceableTree<Integer> btree = new BalanceableTree<Integer>() {
        };
        Node<Integer> root = btree.addRoot(0);
        Node<Integer> left = btree.addLeft(root, 1);
        Node<Integer> leftLeft = btree.addLeft(left, 2);
        btree.reduceSubtreeHeight(leftLeft);
        assertEquals((Integer) 1, btree.root().getElement());
    }

}