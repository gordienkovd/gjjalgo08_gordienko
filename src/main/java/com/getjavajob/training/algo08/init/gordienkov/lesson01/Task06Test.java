package com.getjavajob.training.algo08.init.gordienkov.lesson01;

import static com.getjavajob.training.algo08.init.gordienkov.lesson01.Task06.*;
import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class Task06Test {
    public static void main(String[] args) {
        testGetVariantA();
        testGetVariantB();
        testGetVariantC();
        testGetVariantD();
        testGetVariantE();
        testGetVariantF();
        testGetVariantG();
        testGetVariantH();
        testGetVariantI();
    }

    private static void testGetVariantA() {
        assertEquals("Task06Test.testGetVariantA", 0b100, getVariantA(2));
    }

    private static void testGetVariantB() {
        assertEquals("Task06Test.testGetVariantB", 0b1000, getVariantB(2, 2));
    }

    private static void testGetVariantC() {
        assertEquals("Task06Test.testGetVariantC", 0b10, getVariantC(0b11, 1));
    }

    private static void testGetVariantD() {
        assertEquals("Task06Test.testGetVariantD", 0b110, getVariantD(0b100, 1));
    }

    private static void testGetVariantE() {
        assertEquals("Task06Test.testGetVariantE", 0b110, getVariantE(0b100, 1));
    }

    private static void testGetVariantF() {
        assertEquals("Task06Test.testGetVariantF", 0b100, getVariantF(0b110, 1));
    }

    private static void testGetVariantG() {
        assertEquals("Task06Test.testGetVariantG", 0b10, getVariantG(0b1010, 2));
    }

    private static void testGetVariantH() {
        assertEquals("Task06Test.testGetVariantH", 0b1, getVariantH(0b1010, 1));
    }

    private static void testGetVariantI() {
        assertEquals("Task06Test.testGetVariantI", "00000010", getVariantI((byte) 0b10));
    }
}
