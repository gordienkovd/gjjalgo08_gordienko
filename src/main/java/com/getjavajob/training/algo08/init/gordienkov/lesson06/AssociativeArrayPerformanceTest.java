package com.getjavajob.training.algo08.init.gordienkov.lesson06;

import com.getjavajob.training.algo08.util.StopWatch;

import java.util.HashMap;
import java.util.Map;

import static com.getjavajob.training.algo08.util.ConsoleHelper.println;

public class AssociativeArrayPerformanceTest {
    public static void main(String[] args) {
        putHashMap();
        addAssociativeArray();
        removeHashMap();
        removeAssociativeArray();
    }

    private static void putHashMap() {
        long start = StopWatch.start();
        Map<Integer, Integer> hm = new HashMap<>();
        for (int i = 0; i < 7000000; i++) {
            hm.put(i, 1);
        }
        println("HashMap.put(): " + StopWatch.getElapsedTime(start) + " ms");
    }

    private static void addAssociativeArray() {
        long start = StopWatch.start();
        AssociativeArray<Integer, Integer> ar = new AssociativeArray<>();
        for (int i = 0; i < 7000000; i++) {
           ar.add(i, 1);
        }
        println("AssociativeArray.add(): " + StopWatch.getElapsedTime(start) + " ms");
    }

    private static void removeHashMap() {
        long start = StopWatch.start();
        Map<Integer, Integer> hm = new HashMap<>();
        for (int i = 0; i < 7000000; i++) {
            hm.put(i, 1);
        }
        for (int i = 0; i < 7000000; i++) {
            hm.remove(i);
        }
        println("HashMap.remove(): " + StopWatch.getElapsedTime(start) + " ms");
    }

    private static void removeAssociativeArray() {
        long start = StopWatch.start();
        AssociativeArray<Integer, Integer> ar = new AssociativeArray<>();
        for (int i = 0; i < 7000000; i++) {
            ar.add(i, 1);
        }
        for (int i = 0; i < 7000000; i++) {
            ar.remove(i);
        }
        println("AssociativeArray.add(): " + StopWatch.getElapsedTime(start) + " ms");
    }
}
