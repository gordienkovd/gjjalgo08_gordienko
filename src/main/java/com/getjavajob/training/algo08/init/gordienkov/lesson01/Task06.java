package com.getjavajob.training.algo08.init.gordienkov.lesson01;

import static com.getjavajob.training.algo08.util.ConsoleHelper.*;

public class Task06 {
    public static void main(String[] args) {
        println("Variant A");
        println(getVariantA(getIntNumberInRange(-1, 32)));
        println("Variant B");
        println(getVariantB(getIntNumberInRange(-1, 32), getIntNumberInRange(-1, 32)));
        println("Variant C");
        println(getVariantC(getIntNumber(), getIntNumberInRange(0, 32)));
        println("Variant D");
        println(getVariantD(getIntNumber(), getIntNumberInRange(0, 32)));
        println("Variant E");
        println(getVariantE(getIntNumber(), getIntNumberInRange(0, 32)));
        println("Variant F");
        println(getVariantF(getIntNumber(), getIntNumberInRange(0, 32)));
        println("Variant G");
        println(getVariantG(getIntNumber(), getIntNumberInRange(0, 32)));
        println("Variant H");
        println(getVariantH(getIntNumber(), getIntNumberInRange(0, 32)));
        println("Variant I");
        println(getVariantI(readByte()));
    }

    /**
     * @param n - number of bits in 0-31
     * @return 2^n
     */
    static int getVariantA(int n) {
        return 0b01 << n;
    }

    /**
     * @param n - number of bits in 0-31
     * @param m - number of bits in 0-31
     * @return 2^n+2^m
     */
    static int getVariantB(int n, int m) {
        if (n == m) {
            return (0b10 << n) | (0b010 << m);
        } else {
            return (0b01 << n) | (0b01 << m);
        }
    }

    /**
     * @param a - number
     * @param n - number of bits in 1-31
     * @return number where reset n lower bits
     */
    static int getVariantC(int a, int n) {
        return a & (0b01 << n);
    }

    /**
     * @param a - number
     * @param n - number of bit in 1-31
     * @return number where set n-th bit with 1
     */
    static int getVariantD(int a, int n) {
        return a | (0b01 << n);
    }

    /**
     * @param a - number
     * @param n - number of bit in 1-31
     * @return number where inverted n-th bit
     */
    static int getVariantE(int a, int n) {
        return a ^ (0b01 << n);
    }

    /**
     * @param a - number
     * @param n - number of bit in 1-31
     * @return number where set n-th bit with 0
     */
    static int getVariantF(int a, int n) {
        return (a | 0b01 << n) ^ (0b01 << n);
    }

    /**
     * @param a - number
     * @param n - number of bits in 1-31
     * @return n lower bits
     */
    static int getVariantG(int a, int n) {
        return a & (a >>> n);
    }

    /**
     * @param a - number
     * @param n - number of bit in 1-31
     * @return n-th bit of a
     */
    static int getVariantH(int a, int n) {
        return (a >> n) & 0b01;
    }

    /**
     * @param a - number
     * @return bin representation
     */
    static String getVariantI(byte a) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < 8; i++) {
            if (((1 << i) & a) != 0) {
                result.append("1");
            } else {
                result.append("0");
            }
        }
        return result.reverse().toString();
    }
}