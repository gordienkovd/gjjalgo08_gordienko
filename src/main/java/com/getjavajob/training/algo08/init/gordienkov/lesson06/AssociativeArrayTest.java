package com.getjavajob.training.algo08.init.gordienkov.lesson06;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AssociativeArrayTest {
    @Test
    public void add() {
        AssociativeArray<Integer, String> actual = new AssociativeArray<>(3);
        actual.add(null, "a");
        actual.add(1, "b");
        actual.add(2, "c");
        actual.add(3, "abc");
        actual.add(4, "d");
        assertEquals("a", actual.get(null));
        assertEquals("b", actual.get(1));
        assertEquals("abc", actual.get(3));
        assertEquals("d", actual.get(4));
        assertEquals("d", actual.add(4, "V"));
    }

    @Test
    public void remove() {
        AssociativeArray<Integer, String> actual = new AssociativeArray<>(3);
        actual.add(null, "a");
        actual.add(1, "b");
        actual.add(2, "c");
        actual.add(3, "abc");
        actual.add(4, null);
        assertEquals("a", actual.remove(null));
        assertEquals("c", actual.remove(2));
        assertEquals("abc", actual.remove(3));
        assertEquals(null, actual.remove(4));
    }

    @Test
    public void get() {
        AssociativeArray<Integer, String> actual = new AssociativeArray<>(3);
        actual.add(null, "a");
        actual.add(1, "b");
        actual.add(2, "c");
        actual.add(3, "abc");
        actual.add(4, null);
        assertEquals("a", actual.get(null));
        assertEquals("b", actual.get(1));
        assertEquals("abc", actual.get(3));
        assertEquals(null, actual.get(4));
    }

    @Test
    public void getNewIndex() {
        AssociativeArray<String, String> actual = new AssociativeArray<>(3);
        actual.add(null, "a");
        actual.add("b", "b");
        actual.add("c", "c");
        actual.add("abc", "abc");
        actual.add("null", null);
        assertEquals(2, actual.getNewIndex("polygenelubricants"));
        assertEquals(5, actual.getNewIndex("random"));
    }
}