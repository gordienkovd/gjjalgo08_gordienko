package com.getjavajob.training.algo08.init.gordienkov.lesson01;

import static com.getjavajob.training.algo08.init.gordienkov.lesson01.Task07.*;
import static com.getjavajob.training.algo08.util.Assert.assertEquals;

public class Task07Test {
    public static void main(String[] args) {
        testSwapValuesOfVars1();
        testSwapValuesOfVars2();
        testSwapValuesOfVars3();
        testSwapValuesOfVars4();
    }

    private static void testSwapValuesOfVars1() {
        assertEquals("Task07Test.testSwapValuesOfVars1", 20, swapValuesOfVars1(10, 20));
    }

    private static void testSwapValuesOfVars2() {
        assertEquals("Task07Test.testSwapValuesOfVars2", 23, swapValuesOfVars2(9, 23));
    }

    private static void testSwapValuesOfVars3() {
        assertEquals("Task07Test.testSwapValuesOfVars3", 11, swapValuesOfVars3(2, 11));
    }

    private static void testSwapValuesOfVars4() {
        assertEquals("Task07Test.testSwapValuesOfVars4", 2, swapValuesOfVars4(25, 2));
    }
}
