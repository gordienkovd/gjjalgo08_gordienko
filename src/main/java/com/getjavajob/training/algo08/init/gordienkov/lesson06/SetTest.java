package com.getjavajob.training.algo08.init.gordienkov.lesson06;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class SetTest {
    @Test
    public void add() {
        Set<String> expected = new HashSet<>();
        expected.add("1");
        expected.add("1");
        assertEquals(1, expected.size());
    }

    @Test
    public void addAll() {
        Set<String> expected = new HashSet<>();
        List<String> actual = new ArrayList<>();
        actual.add("1");
        actual.add("1");
        actual.add("1");
        expected.addAll(actual);
        assertEquals(1, expected.size());
    }
}
