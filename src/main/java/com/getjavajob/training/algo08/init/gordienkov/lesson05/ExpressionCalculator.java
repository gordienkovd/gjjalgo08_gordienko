package com.getjavajob.training.algo08.init.gordienkov.lesson05;

import java.util.ArrayDeque;
import java.util.Deque;

import static com.getjavajob.training.algo08.util.ConsoleHelper.println;
import static com.getjavajob.training.algo08.util.ConsoleHelper.readLine;

public class ExpressionCalculator {
    private static final String OPERATORS = "-+/*";
    private static final String OPERANDS = "0123456789";

    public int getPrecedence(char operator) {
        switch (operator) {
            case '-':
            case '+':
                return 1;
            case '*':
            case '/':
                return 2;
            default:
                return 0;
        }
    }

    public boolean equalsOperators(char firstOperator, char secondOperator) {
        return getPrecedence(firstOperator) >= getPrecedence(secondOperator);
    }

    public boolean isOperator(char symbol) {
        return OPERATORS.indexOf(symbol) >= 0;
    }

    public boolean isOperand(char symbol) {
        return OPERANDS.indexOf(symbol) >= 0;
    }

    public String infixToPostfix(String infixNotation) {
        char[] elements = infixNotation.toCharArray();
        Deque<Character> operators = new ArrayDeque<>();
        StringBuilder postfixNotation = new StringBuilder(infixNotation.length());
        for (char element : elements) {
            if (isOperator(element)) {
                while (!operators.isEmpty() && operators.peek() != '(') {
                    if (equalsOperators(operators.peek(), element)) {
                        postfixNotation.append(operators.pop());
                    } else {
                        break;
                    }
                }
                operators.push(element);
            } else if (element == '(') {
                operators.push(element);
            } else if (element == ')') {
                while (!operators.isEmpty() && operators.peek() != '(') {
                    postfixNotation.append(operators.pop());
                }
                if (!operators.isEmpty()) {
                    operators.pop();
                }
            } else if (isOperand(element)) {
                postfixNotation.append(element);
            }
        }
        while (operators.size() != 0) {
            postfixNotation.append(operators.pop());
        }
        return postfixNotation.toString();
    }

    public int calculatePostfix(String postfixNotation) {
        char[] elements = postfixNotation.toCharArray();
        Deque<Integer> stack = new ArrayDeque<>();
        for (char element : elements) {
            if (isOperand(element)) {
                stack.push(element - '0');
            } else if (isOperator(element)) {
                int firstOperator = stack.pop();
                int secondOperator = stack.pop();
                int result;
                switch (element) {
                    case '*':
                        result = firstOperator * secondOperator;
                        stack.push(result);
                        break;
                    case '/':
                        result = secondOperator / firstOperator;
                        stack.push(result);
                        break;
                    case '+':
                        result = firstOperator + secondOperator;
                        stack.push(result);
                        break;
                    case '-':
                        result = secondOperator - firstOperator;
                        stack.push(result);
                        break;
                }
            }
        }
        return stack.pop();
    }

    public static void main(String[] args) {
        ExpressionCalculator calculator = new ExpressionCalculator();
        String expression = readLine();
        String postfix = calculator.infixToPostfix(expression);
        println("Postfix notation: " + postfix);
        println("Calculated postfix notation: " + calculator.calculatePostfix(postfix));
    }
}