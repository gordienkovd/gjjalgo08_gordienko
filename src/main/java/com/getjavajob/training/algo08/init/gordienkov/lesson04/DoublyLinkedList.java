package com.getjavajob.training.algo08.init.gordienkov.lesson04;

import java.util.*;

public class DoublyLinkedList<V> extends AbstractSequentialList<V> implements List<V> {
    private int size;
    private Element<V> first;
    private Element<V> last;

    public boolean add(V val) {
        linkLast(val);
        return true;
    }

    public void add(int index, V element) {
        checkPositionIndex(index);
        if (index == size) {
            linkLast(element);
        } else {
            linkBefore(element, node(index));
        }
    }

    public boolean remove(Object o) {
        if (o == null) {
            for (Element<V> x = first; x != null; x = x.next) {
                if (x.val == null) {
                    unlink(x);
                    return true;
                }
            }
        } else {
            for (Element<V> x = first; x != null; x = x.next) {
                if (o.equals(x.val)) {
                    unlink(x);
                    return true;
                }
            }
        }
        return false;
    }

    public V remove(int index) {
        checkElementIndex(index);
        return unlink(node(index));
    }

    public V get(int index) {
        checkElementIndex(index);
        return node(index).val;
    }

    private V unlink(Element<V> x) {
        final V element = x.val;
        final Element<V> next = x.next;
        final Element<V> prev = x.prev;
        if (prev == null) {
            first = next;
        } else {
            prev.next = next;
            x.prev = null;
        }
        if (next == null) {
            last = prev;
        } else {
            next.prev = prev;
            x.next = null;
        }
        x.val = null;
        size--;
        modCount++;
        return element;
    }

    private void linkLast(V e) {
        final Element<V> l = last;
        final Element<V> newNode = new Element<>(l, e, null);
        last = newNode;
        if (l == null) {
            first = newNode;
        } else {
            l.next = newNode;
        }
        size++;
        modCount++;
    }

    private void linkBefore(V e, Element<V> succ) {
        final Element<V> pred = succ.prev;
        final Element<V> newNode = new Element<>(pred, e, succ);
        succ.prev = newNode;
        if (pred == null) {
            first = newNode;
        } else {
            pred.next = newNode;
        }
        size++;
        modCount++;
    }

    private Element<V> node(int index) {
        if (index < (size >> 1)) {
            Element<V> x = first;
            for (int i = 0; i < index; i++) {
                x = x.next;
            }
            return x;
        } else {
            Element<V> x = last;
            for (int i = size - 1; i > index; i--) {
                x = x.prev;
            }
            return x;
        }
    }

    private void checkPositionIndex(int index) {
        if (!isPositionIndex(index)) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
        }
    }

    private void checkElementIndex(int index) {
        if (!isElementIndex(index)) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
        }
    }

    private boolean isPositionIndex(int index) {
        return index >= 0 && index <= size;
    }

    private boolean isElementIndex(int index) {
        return index >= 0 && index < size;
    }

    private String outOfBoundsMsg(int index) {
        return "Index: " + index + ", Size: " + size;
    }

    public int size() {
        return size;
    }

    public ListIterator<V> listIterator(int index) {
        checkPositionIndex(index);
        return new ListIteratorImpl(index);
    }

    private static class Element<V> {
        V val;
        Element<V> next;
        Element<V> prev;

        Element(Element<V> prev, V element, Element<V> next) {
            this.val = element;
            this.next = next;
            this.prev = prev;
        }
    }

    private class ListIteratorImpl implements ListIterator<V> {
        private Element<V> lastReturned = null;
        private Element<V> next;
        private int nextIndex;
        private int expectedModCount = modCount;

        ListIteratorImpl(int index) {
            next = (index == size) ? null : node(index);
            nextIndex = index;
        }

        public boolean hasNext() {
            return nextIndex < size;
        }

        public V next() {
            checkForComodification();
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            lastReturned = next;
            next = next.next;
            nextIndex++;
            return lastReturned.val;
        }

        public boolean hasPrevious() {
            return nextIndex > 0;
        }

        public V previous() {
            checkForComodification();
            if (!hasPrevious()) {
                throw new NoSuchElementException();
            }
            lastReturned = next = (next == null) ? last : next.prev;
            nextIndex--;
            return lastReturned.val;
        }

        public int nextIndex() {
            return nextIndex;
        }

        public int previousIndex() {
            return nextIndex - 1;
        }

        public void remove() {
            checkForComodification();
            if (lastReturned == null) {
                throw new IllegalStateException();
            }
            Element<V> lastNext = lastReturned.next;
            unlink(lastReturned);
            if (next == lastReturned) {
                next = lastNext;
            } else {
                nextIndex--;
            }
            lastReturned = null;
            expectedModCount++;
        }

        public void set(V e) {
            if (lastReturned == null) {
                throw new IllegalStateException();
            }
            checkForComodification();
            lastReturned.val = e;
        }

        public void add(V e) {
            checkForComodification();
            lastReturned = null;
            if (next == null) {
                linkLast(e);
            } else {
                linkBefore(e, next);
            }
            nextIndex++;
            expectedModCount++;
        }

        final void checkForComodification() {
            if (modCount != expectedModCount) {
                throw new ConcurrentModificationException();
            }
        }
    }
}
