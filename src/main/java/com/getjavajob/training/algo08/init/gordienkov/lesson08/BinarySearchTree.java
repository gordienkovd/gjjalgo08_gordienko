package com.getjavajob.training.algo08.init.gordienkov.lesson08;

import com.getjavajob.training.algo08.init.gordienkov.lesson07.LinkedBinaryTree;
import com.getjavajob.training.algo08.init.gordienkov.lesson07.Node;

import java.util.Comparator;

/**
 * @author Vital Severyn
 * @since 31.07.15
 */
public class BinarySearchTree<E> extends LinkedBinaryTree<E> {
    private Comparator<E> comparator;

    public BinarySearchTree() {
    }

    public BinarySearchTree(Comparator<E> comparator) {
        this.comparator = comparator;
    }

    /**
     * Method for comparing two values
     *
     * @param val1
     * @param val2
     * @return
     */
    protected int compare(E val1, E val2) {
        if (comparator == null) {
            Comparable<E> comparable = (Comparable<E>) val1;
            Comparable<E> comparable2 = (Comparable<E>) val2;
            return comparable.compareTo((E) comparable2);
        } else {
            return comparator.compare(val1, val2);
        }
    }

    /**
     * Returns the node in n's subtree by val
     *
     * @param n
     * @param val
     * @return
     */
    public Node<E> treeSearch(Node<E> n, E val) {
        if (n == null) {
            return null;
        } else if (n.getElement() == val) {
            return n;
        } else {
            if (compare(n.getElement(), val) > 0) {
                return treeSearch(left(n), val);
            } else {
                return treeSearch(right(n), val);
            }
        }
    }

    protected void afterElementRemoved(Node<E> n) {

    }

    protected void afterElementAdded(Node<E> n) {

    }

}
