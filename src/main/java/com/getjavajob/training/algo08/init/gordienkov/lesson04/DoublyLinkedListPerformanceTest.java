package com.getjavajob.training.algo08.init.gordienkov.lesson04;

import com.getjavajob.training.algo08.util.StopWatch;

import java.util.LinkedList;
import java.util.List;

import static com.getjavajob.training.algo08.util.ConsoleHelper.println;

public class DoublyLinkedListPerformanceTest {
    public static void main(String[] args) {
        addToBeginningDoublyLinkedList();
        addToBeginningLinkedList();
        addToMiddleDoublyLinkedList();
        addToMiddleLinkedList();
        addToEndDoublyLinkedList();
        addToEndLinkedList();
        removeFromBeginningDoublyLinkedList();
        removeFromBeginningLinkedList();
        removeFromMiddleDoublyLinkedList();
        removeFromMiddleLinkedList();
        removeFromEndDoublyLinkedList();
        removeFromEndLinkedList();
    }

    private static void addToBeginningDoublyLinkedList() {
        long start = StopWatch.start();
        List<String> dll = new DoublyLinkedList<>();
        for (int i = 0; i < 40000000; i++) {
            dll.add(0, "a");
        }
        println("DoublyLinkedList.add(): " + StopWatch.getElapsedTime(start) + " ms");
    }

    private static void addToBeginningLinkedList() {
        long start = StopWatch.start();
        List<String> ll = new LinkedList<>();
        for (int i = 0; i < 40000000; i++) {
            ll.add(0, "a");
        }
        println("LinkedList.add(): " + StopWatch.getElapsedTime(start) + " ms");
    }

    private static void addToMiddleDoublyLinkedList() {
        long start = StopWatch.start();
        List<String> dll = new DoublyLinkedList<>();
        for (int i = 0; i < 100000; i++) {
            dll.add(dll.size() / 2, "a");
        }
        println("DoublyLinkedList.add(): " + StopWatch.getElapsedTime(start) + " ms");
    }

    private static void addToMiddleLinkedList() {
        long start = StopWatch.start();
        List<String> ll = new LinkedList<>();
        for (int i = 0; i < 100000; i++) {
            ll.add(ll.size() / 2, "a");
        }
        println("LinkedList.add(): " + StopWatch.getElapsedTime(start) + " ms");
    }

    private static void addToEndDoublyLinkedList() {
        long start = StopWatch.start();
        List<String> dll = new DoublyLinkedList<>();
        for (int i = 0; i < 10000000; i++) {
            dll.add("a");
        }
        println("DoublyLinkedList.add(): " + StopWatch.getElapsedTime(start) + " ms");
    }

    private static void addToEndLinkedList() {
        long start = StopWatch.start();
        List<String> ll = new LinkedList<>();
        for (int i = 0; i < 10000000; i++) {
            ll.add("a");
        }
        println("LinkedList.add(): " + StopWatch.getElapsedTime(start) + " ms");
    }

    private static void removeFromBeginningDoublyLinkedList() {
        List<String> dll = new DoublyLinkedList<>();
        for (int i = 0; i < 100000000; i++) {
            dll.add("a");
        }
        long start = StopWatch.start();
        for (int i = 0; i < 100000000; i++) {
            dll.remove(0);
        }
        println("DoublyLinkedList.remove(): " + StopWatch.getElapsedTime(start) + " ms");
    }

    private static void removeFromBeginningLinkedList() {
        List<String> ll = new LinkedList<>();
        for (int i = 0; i < 100000000; i++) {
            ll.add("a");
        }
        long start = StopWatch.start();
        for (int i = 0; i < 100000000; i++) {
            ll.remove(0);
        }
        println("LinkedList.remove(): " + StopWatch.getElapsedTime(start) + " ms");
    }

    private static void removeFromMiddleDoublyLinkedList() {
        List<String> dll = new DoublyLinkedList<>();
        for (int i = 0; i < 100000; i++) {
            dll.add("a");
        }
        long start = StopWatch.start();
        for (int i = 0; i < 100000; i++) {
            dll.remove(dll.size() / 2);
        }
        println("DoublyLinkedList.remove(): " + StopWatch.getElapsedTime(start) + " ms");
    }

    private static void removeFromMiddleLinkedList() {
        List<String> ll = new LinkedList<>();
        for (int i = 0; i < 100000; i++) {
            ll.add("a");
        }
        long start = StopWatch.start();
        for (int i = 0; i < 100000; i++) {
            ll.remove(ll.size() / 2);
        }
        println("LinkedList.remove(): " + StopWatch.getElapsedTime(start) + " ms");
    }

    private static void removeFromEndDoublyLinkedList() {
        List<String> dll = new DoublyLinkedList<>();
        for (int i = 0; i < 100000000; i++) {
            dll.add("a");
        }
        long start = StopWatch.start();
        for (int i = 0; i < 100000000; i++) {
            dll.remove(dll.size() - 1);
        }
        println("DoublyLinkedList.remove(): " + StopWatch.getElapsedTime(start) + " ms");
    }

    private static void removeFromEndLinkedList() {
        List<String> ll = new LinkedList<>();
        for (int i = 0; i < 100000000; i++) {
            ll.add("a");
        }
        long start = StopWatch.start();
        for (int i = 0; i < 100000000; i++) {
            ll.remove(ll.size() - 1);
        }
        println("LinkedList.remove(): " + StopWatch.getElapsedTime(start) + " ms");
    }
}
