package com.getjavajob.training.algo08.init.gordienkov.lesson10;

import org.junit.Test;

import static com.getjavajob.training.algo08.init.gordienkov.lesson10.QuickSort.*;
import static org.junit.Assert.assertArrayEquals;

public class QuickSortTest {
    @Test
    public void quickSortTest() throws Exception {
        int[] expected = new int[Integer.MAX_VALUE / 2];
        for (int i = 0; i < expected.length; i++) {
            expected[i] = expected.length - i;
        }
        int[] actual = new int[Integer.MAX_VALUE / 2];
        for (int i = 0; i < actual.length; i++) {
            actual[i] = i + 1;
        }
        quickSort(expected);
        assertArrayEquals(actual, expected);
    }
}