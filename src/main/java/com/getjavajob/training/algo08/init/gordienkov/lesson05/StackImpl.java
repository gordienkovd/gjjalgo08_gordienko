package com.getjavajob.training.algo08.init.gordienkov.lesson05;

public interface StackImpl<E> {
    void push(E e);

    E pop();
}
