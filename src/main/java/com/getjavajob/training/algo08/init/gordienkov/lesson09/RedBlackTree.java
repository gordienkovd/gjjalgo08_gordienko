package com.getjavajob.training.algo08.init.gordienkov.lesson09;

import com.getjavajob.training.algo08.init.gordienkov.lesson07.Node;
import com.getjavajob.training.algo08.init.gordienkov.lesson08.BalanceableTree;

import java.awt.*;

public class RedBlackTree<E> extends BalanceableTree<E> {
    private final static Color BLACK = Color.BLACK;
    private final static Color RED = Color.RED;

    static class NodeRBT<E> extends NodeImpl<E> {
        private Color color = BLACK;

        NodeRBT(E element, Node<E> parent, Node<E> left, Node<E> right) {
            super(element, parent, left, right);
        }
    }

    @Override
    protected NodeRBT<E> validate(Node<E> n) throws IllegalArgumentException {
        if (n == null) {
            return null;
        }
        if (n instanceof NodeRBT) {
            return (NodeRBT<E>) n;
        } else {
            throw new IllegalArgumentException();
        }
    }

    private boolean isBlack(Node<E> n) {
        return n != null && validate(n).color == BLACK;
    }

    private boolean isRed(Node<E> n) {
        return n != null && validate(n).color == RED;
    }

    private void makeBlack(Node<E> n) throws NullPointerException {
        if (n != null)
            validate(n).color = BLACK;
    }

    private void makeRed(Node<E> n) {
        if (n != null)
            validate(n).color = RED;
    }

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        if (size != 0) {
            throw new IllegalStateException("Root Node already exists");
        } else {
            root = new NodeRBT<>(e, null, null, null);
            size++;
        }
        afterElementAdded(root);
        return root;
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        if (validate(n).getLeft() != null & validate(n).getRight() != null) {
            throw new IllegalArgumentException("Node have two children");
        }
        NodeRBT<E> child = new NodeRBT<>(e, validate(n), null, null);
        if (validate(n).getLeft() != null) {
            validate(n).setRight(child);
            size++;
        } else {
            validate(n).setLeft(child);
            size++;
        }
        afterElementAdded(child);
        return child;
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        NodeRBT<E> child = new NodeRBT<>(e, validate(n), null, null);
        if (validate(n).getLeft() != null) {
            throw new IllegalArgumentException("Left Node already exists");
        } else {
            validate(n).left = child;
            size++;
        }
        afterElementAdded(child);
        return child;
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        NodeRBT<E> child = new NodeRBT<>(e, validate(n), null, null);
        if (validate(n).getRight() != null) {
            throw new IllegalArgumentException("Right Node already exists");
        } else {
            validate(n).right = child;
            size++;
        }
        afterElementAdded(child);
        return child;
    }

    @Override
    public E remove(Node<E> n) {
        if (validate(n).left != null && validate(n).right != null) {
            NodeRBT<E> s = successor(validate(n));
            validate(n).element = s.element;
            n = s;
        }
        NodeRBT<E> replacement = (validate(validate(n).left) != null ? validate(validate(n).left) : validate(validate(n).right));
        if (replacement != null) {
            replacement.parent = validate(n).parent;
            if (validate(n).parent == null) {
                root = replacement;
            } else if (n == validate(n).parent.left) {
                validate(n).parent.left = replacement;
            } else {
                validate(n).parent.right = replacement;
            }
            validate(n).left = validate(n).right = validate(n).parent = null;
            if (validate(n).color == BLACK) {
                afterElementRemoved(replacement);
            }
        } else if (validate(n).parent == null) {
            root = null;
        } else {
            if (validate(n).color == BLACK) {
                afterElementRemoved(n);
            }
            if (validate(n).parent != null) {
                if (n == validate(n).parent.left) {
                    validate(n).parent.left = null;
                } else if (validate(n) == validate(n).parent.right) {
                    validate(n).parent.right = null;
                }
                validate(n).parent = null;
            }
        }
        size--;
        return validate(n).element;
    }

    @Override
    protected void afterElementAdded(Node<E> n) {
        NodeRBT<E> node = validate(n);
        node.color = RED;
        while (node != null && node != root && validate(node.parent).color == RED) {
            if (parent(node) == left(parent(parent(node)))) {
                NodeRBT<E> rightParentSibling = (NodeRBT<E>) right(parent(parent(node)));
                if (isRed(rightParentSibling)) {
                    makeBlack(parent(node));
                    makeBlack(rightParentSibling);
                    makeRed(parent(parent(node)));
                    node = (NodeRBT<E>) parent(parent(node));
                } else {
                    if (node == right(parent(node))) {
                        node = (NodeRBT<E>) parent(node);
                        rotateLeft(node);
                    }
                    makeBlack(parent(node));
                    makeRed(parent(parent(node)));
                    rotateRight(validate(parent(parent(node))));
                }
            } else {
                NodeRBT<E> leftParentSibling = (NodeRBT<E>) left(parent(parent(node)));
                if (isRed(leftParentSibling)) {
                    makeBlack(parent(node));
                    makeBlack(leftParentSibling);
                    makeRed(parent(parent(node)));
                    node = (NodeRBT<E>) parent(parent(node));
                } else {
                    if (node == left(parent(node))) {
                        node = (NodeRBT<E>) parent(node);
                        rotateRight(node);
                    }
                    makeBlack(parent(node));
                    makeRed(parent(parent(node)));
                    rotateLeft(validate(parent(parent(node))));
                }
            }
        }
        makeBlack(root());
    }

    @Override
    protected void afterElementRemoved(Node<E> n) {
        while (n != root && isBlack(n)) {
            if (n == leftOf(parentOf(n))) {
                Node<E> sibling = rightOf(parentOf(n));
                if (isRed(sibling)) {
                    makeBlack(sibling);
                    makeRed(parentOf(n));
                    reduceSubtreeHeight(parentOf(n));
                    sibling = rightOf(parentOf(n));
                }
                if (isBlack(leftOf(sibling)) && isBlack(rightOf(sibling))) {
                    makeRed(sibling);
                    n = parentOf(n);
                } else {
                    if (isBlack(rightOf(sibling))) {
                        makeBlack(leftOf(sibling));
                        makeRed(sibling);
                        reduceSubtreeHeight(sibling);
                        sibling = rightOf(parentOf(n));
                    }
                    setColor(sibling, colorOf(parentOf(n)));
                    makeBlack(parentOf(n));
                    makeBlack(rightOf(sibling));
                    reduceSubtreeHeight(parentOf(n));
                    n = root;
                }
            } else {
                Node<E> sibling = leftOf(parentOf(n));
                if (isRed(sibling)) {
                    makeBlack(sibling);
                    makeRed(parentOf(n));
                    reduceSubtreeHeight(parentOf(n));
                    sibling = leftOf(parentOf(n));
                }
                if (isBlack(leftOf(sibling)) && isBlack(rightOf(sibling))) {
                    setColor(sibling, Color.red);
                    n = parentOf(n);
                } else {
                    if (isBlack(leftOf(sibling))) {
                        makeBlack(rightOf(sibling));
                        makeRed(sibling);
                        reduceSubtreeHeight(sibling);
                        sibling = leftOf(parentOf(n));
                    }
                    setColor(sibling, colorOf(parentOf(n)));
                    makeBlack(parentOf(n));
                    makeBlack(leftOf(sibling));
                    reduceSubtreeHeight(parentOf(n));
                    n = root;
                }
            }
        }
        makeBlack(n);
    }

    private Node<E> parentOf(Node<E> n) {
        if (n == null) {
            return null;
        } else {
            return validate(n).getParent();
        }
    }

    private Node<E> leftOf(Node<E> n) {
        if (n == null) {
            return null;
        } else {
            return validate(n).getLeft();
        }
    }

    private Node<E> rightOf(Node<E> n) {
        if (n == null) {
            return null;
        } else {
            return validate(n).getRight();
        }
    }

    private Color colorOf(Node<E> n) {
        if (n == null) {
            return BLACK;
        } else {
            return validate(n).color;
        }
    }

    private void setColor(Node<E> n, Color c) {
        if (n != null) {
            validate(n).color = c;
        }
    }

    private void rotateLeft(NodeRBT<E> n) {
        if (n != null) {
            NodeRBT<E> r = (NodeRBT<E>) n.right;
            n.right = r.left;
            if (r.left != null) {
                r.left.parent = n;
            }
            r.parent = n.parent;
            if (n.parent == null) {
                root = r;
            } else if (n.parent.left == n) {
                n.parent.left = r;
            } else {
                n.parent.right = r;
            }
            r.left = n;
            n.parent = r;
        }
    }

    private void rotateRight(NodeRBT<E> n) {
        if (n != null) {
            NodeRBT<E> l = (NodeRBT<E>) n.left;
            n.left = l.right;
            if (l.right != null) {
                l.right.parent = n;
            }
            l.parent = n.parent;
            if (n.parent == null) {
                root = l;
            } else if (n.parent.right == n) {
                n.parent.right = l;
            } else {
                n.parent.left = l;
            }
            l.right = n;
            n.parent = l;
        }
    }

    private NodeRBT<E> successor(NodeRBT<E> t) {
        if (t == null) {
            return null;
        } else if (t.right != null) {
            NodeRBT<E> p = validate(t.right);
            while (p.left != null)
                p = validate(p.left);
            return p;
        } else {
            NodeRBT<E> p = validate(t.parent);
            NodeRBT<E> ch = t;
            while (p != null && ch == p.right) {
                ch = p;
                p = validate(p.parent);
            }
            return p;
        }
    }
}
