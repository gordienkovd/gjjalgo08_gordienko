package com.getjavajob.training.algo08.init.gordienkov.lesson09;

import java.util.*;

public class CitySearchEngine {
    public static SortedSet<String> searchCities(NavigableSet<String> storage, String subString) throws NullPointerException {
        char lastLetter = subString.charAt(subString.length() - 1);
        if (lastLetter == '\uffff') {
            return null;
        }
        return storage.subSet(subString, subString.replace(lastLetter, (char) (lastLetter + 1)));
    }

    public static NavigableSet<String> fillStorage(Collection<String> cityNames) throws NullPointerException {
        if (cityNames == null) {
            throw new NullPointerException("CityNames is Empty");
        }
        NavigableSet<String> storage = new TreeSet<>(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareToIgnoreCase(o2);
            }
        });
        for (String element : cityNames) {
            storage.add(element);
        }
        return storage;
    }
}
