package com.getjavajob.training.algo08.init.gordienkov.lesson04;

import java.util.ArrayList;
import java.util.List;

public class SinglyLinkedList<E> {
    private Node<E> head;

    public void add(E e) {
        if (head == null) {
            head = new Node<>();
            head.val = e;
        } else if (head.next == null) {
            Node<E> node = new Node<>();
            node.val = e;
            head.next = node;
        } else {
            Node<E> node = head;
            while (node.next != null) {
                node = node.next;
            }
            node.next = new Node<>();
            node.next.val = e;
        }
    }

    public void reverse() {
        Node<E> curr = head;
        Node<E> pre = null;
        Node<E> incoming;
        while (curr != null) {
            incoming = curr.next;
            curr.next = pre;
            pre = curr;
            curr = incoming;
        }
        head = pre;
    }

    public void swap(E e, E v) {
        Node<E> node = head;
        while (node.next != null) {
            if (node.val.equals(e)) {
                node.val = v;
            } else if (node.val.equals(v)) {
                node.val = e;
            }
            node = node.next;
        }
    }

    public List<E> asList() {
        List<E> list = new ArrayList<>();
        Node<E> node = head;
        while (node != null) {
            list.add(node.val);
            node = node.next;
        }
        return list;
    }

    private static class Node<E> {
        Node<E> next;
        E val;
    }
}



