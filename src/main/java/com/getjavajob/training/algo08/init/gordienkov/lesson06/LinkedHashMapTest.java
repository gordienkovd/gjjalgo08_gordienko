package com.getjavajob.training.algo08.init.gordienkov.lesson06;

import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedHashMap;
import java.util.Map;

import static com.getjavajob.training.algo08.util.ConsoleHelper.println;

public class LinkedHashMapTest {
    @Test
    public void put() {
        Map<String, String> expected = new LinkedHashMap<>();
        expected.put("1", "1");
        expected.put("2", "2");
        expected.put("3", "3");
        println(expected.toString());
        Assert.assertEquals("{1=1, 2=2, 3=3}",expected.toString());
    }
}
