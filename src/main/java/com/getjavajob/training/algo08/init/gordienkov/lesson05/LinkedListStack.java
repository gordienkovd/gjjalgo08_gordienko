package com.getjavajob.training.algo08.init.gordienkov.lesson05;

import java.util.ArrayList;
import java.util.List;

public class LinkedListStack <E> implements StackImpl<E> {
    private Node<E> head;

    public void add(E e) {
        if (head == null) {
            head = new Node<>();
            head.val = e;
        } else if (head.next == null) {
            Node<E> node = new Node<>();
            node.val = e;
            head.next = node;
        } else {
            Node<E> node = head;
            while (node.next != null) {
                node = node.next;
            }
            node.next = new Node<>();
            node.next.val = e;
        }
    }

    public void reverse() {
        Node<E> node = head;
        int count = 1;
        while (node.next != null) {
            count++;
            node = node.next;
        }
        Node<E> firstNode = head;
        Node<E> secondNode = head;
        int firstCount = 0;
        int secondCount = count - 1;
        E expVal;
        for (int i = 0; i < count / 2; i++) {
            for (int f = 0; f < firstCount; f++) {
                firstNode = firstNode.next;
            }
            firstCount++;
            for (int s = 0; s < secondCount; s++) {
                secondNode = secondNode.next;
            }
            secondCount--;
            expVal = firstNode.val;
            firstNode.val = secondNode.val;
            secondNode.val = expVal;
            firstNode = head;
            secondNode = head;
        }
    }

    public void swap(E e, E v) {
        Node<E> node = head;
        while (node.next != null) {
            if (node.val.equals(e)) {
                node.val = v;
            } else if (node.val.equals(v)) {
                node.val = e;
            }
            node = node.next;
        }
    }

    public List<E> asList() {
        List<E> list = new ArrayList<>();
        Node<E> node = head;
        while (node != null) {
            list.add(node.val);
            node = node.next;
        }
        return list;
    }

    @Override
    public void push(E e) {
        Node<E> node = new Node<>();
        node.next = head;
        head = node;
        head.val = e;
    }

    @Override
    public E pop() {
        E oldValue = head.next.val;
        head = head.next;
        return oldValue;
    }

    private static class Node<E> {
        Node<E> next;
        E val;
    }
}
