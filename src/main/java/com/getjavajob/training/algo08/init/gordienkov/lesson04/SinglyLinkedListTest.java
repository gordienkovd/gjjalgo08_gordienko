package com.getjavajob.training.algo08.init.gordienkov.lesson04;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class SinglyLinkedListTest {
    @Test
    public void add() throws Exception {
        SinglyLinkedList<Integer> actual = new SinglyLinkedList<>();
        actual.add(1);
        actual.add(2);
        Integer[] expected = new Integer[]{1, 2};
        assertArrayEquals(expected, actual.asList().toArray());
    }

    @Test
    public void reverse() throws Exception {
        SinglyLinkedList<Integer> actual = new SinglyLinkedList<>();
        actual.add(1);
        actual.add(2);
        actual.add(3);
        actual.add(4);
        Integer[] expected = new Integer[]{4, 3, 2, 1};
        actual.reverse();
        assertArrayEquals(expected, actual.asList().toArray());
    }

    @Test
    public void swap() throws Exception {
        SinglyLinkedList<Integer> actual = new SinglyLinkedList<>();
        actual.add(1);
        actual.add(2);
        actual.add(3);
        actual.add(4);
        Integer[] expected = new Integer[]{1, 3, 2, 4};
        actual.swap(3, 2);
        assertArrayEquals(expected, actual.asList().toArray());
    }

}