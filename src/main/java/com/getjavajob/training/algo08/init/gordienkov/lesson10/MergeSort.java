package com.getjavajob.training.algo08.init.gordienkov.lesson10;

import static java.lang.System.arraycopy;

public class MergeSort {
    public static Comparable[] mergeSort(Comparable[] array) {
        if (array.length <= 1) {
            return array;
        }
        Comparable[] first = new Comparable[array.length / 2];
        Comparable[] second = new Comparable[array.length - first.length];
        arraycopy(array, 0, first, 0, first.length);
        arraycopy(array, first.length, second, 0, second.length);
        mergeSort(first);
        mergeSort(second);
        merge(first, second, array);
        return array;
    }

    private static void merge(Comparable[] first, Comparable[] second, Comparable[] result) {
        int iFirst = 0;
        int iSecond = 0;
        int iMerged = 0;
        while (iFirst < first.length && iSecond < second.length) {
            if (first[iFirst].compareTo(second[iSecond]) < 0) {
                result[iMerged] = first[iFirst];
                iFirst++;
            } else {
                result[iMerged] = second[iSecond];
                iSecond++;
            }
            iMerged++;
        }
        arraycopy(first, iFirst, result, iMerged, first.length - iFirst);
        arraycopy(second, iSecond, result, iMerged, second.length - iSecond);
    }
}
