package com.getjavajob.training.algo08.init.gordienkov.lesson08;

import com.getjavajob.training.algo08.init.gordienkov.lesson07.Node;
import org.junit.Test;

import java.util.Comparator;

import static org.junit.Assert.assertEquals;

public class BinarySearchTreeTest {
    @Test
    public void treeSearch() throws Exception {
        BinarySearchTree<Integer> binaryTreeSearch = new BinarySearchTree<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                if (o1 < o2) {
                    return -1;
                } else if (o1 > o2) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });
        Node<Integer> root = binaryTreeSearch.addRoot(0);
        Node<Integer> right = binaryTreeSearch.addRight(binaryTreeSearch.root(), 1);
        Node<Integer> left = binaryTreeSearch.addLeft(binaryTreeSearch.root(), -1);
        Node<Integer> s1 = binaryTreeSearch.treeSearch(root, 1);
        Node<Integer> s2 = binaryTreeSearch.treeSearch(root, -1);
        assertEquals(right, s1);
        assertEquals(left, s2);
        assertEquals(null, binaryTreeSearch.treeSearch(binaryTreeSearch.root(), 2));
    }

    @Test
    public void treeSearch2() throws Exception {
        BinarySearchTree<Integer> binaryTreeSearch = new BinarySearchTree<>();
        Node<Integer> root = binaryTreeSearch.addRoot(0);
        Node<Integer> right = binaryTreeSearch.addRight(binaryTreeSearch.root(), 1);
        Node<Integer> left = binaryTreeSearch.addLeft(binaryTreeSearch.root(), -1);
        Node<Integer> s1 = binaryTreeSearch.treeSearch(root, 1);
        Node<Integer> s2 = binaryTreeSearch.treeSearch(root, -1);
        assertEquals(right, s1);
        assertEquals(left, s2);
        assertEquals(null, binaryTreeSearch.treeSearch(binaryTreeSearch.root(), 2));
    }

    @Test
    public void compare() throws Exception {
        BinarySearchTree<Integer> binaryTreeSearch = new BinarySearchTree<>();
        binaryTreeSearch.addRoot(0);
        Node<Integer> right = binaryTreeSearch.addRight(binaryTreeSearch.root(), 1);
        Node<Integer> left = binaryTreeSearch.addLeft(binaryTreeSearch.root(), -1);
        assertEquals(1, binaryTreeSearch.compare(right.getElement(), left.getElement()));
        assertEquals(-1, binaryTreeSearch.compare(left.getElement(), right.getElement()));
        assertEquals(0, binaryTreeSearch.compare(left.getElement(), left.getElement()));
    }
}