package com.getjavajob.training.algo08.init.gordienkov.lesson09;

import org.junit.Test;

import java.util.NavigableMap;
import java.util.TreeMap;

import static org.junit.Assert.assertEquals;

public class NavigableMapTest {
    @Test
    public void lowerEntry() {
        NavigableMap<Integer, Integer> navigableMap = new TreeMap<>();
        navigableMap.put(1, 1);
        navigableMap.put(2, 2);
        navigableMap.put(3, 3);
        navigableMap.put(4, 4);
        navigableMap.put(5, 5);
        assertEquals("1=1", navigableMap.lowerEntry(2).toString());
        assertEquals(null, navigableMap.lowerEntry(1));
    }

    @Test
    public void lowerKey() {
        NavigableMap<Integer, Integer> navigableMap = new TreeMap<>();
        navigableMap.put(1, 1);
        navigableMap.put(2, 2);
        navigableMap.put(3, 3);
        navigableMap.put(4, 4);
        navigableMap.put(5, 5);
        assertEquals("1", navigableMap.lowerKey(2).toString());
        assertEquals(null, navigableMap.lowerKey(1));
    }

    @Test
    public void floorEntry() {
        NavigableMap<Integer, Integer> navigableMap = new TreeMap<>();
        navigableMap.put(1, 1);
        navigableMap.put(2, 2);
        navigableMap.put(3, 3);
        navigableMap.put(4, 4);
        navigableMap.put(5, 5);
        assertEquals("2=2", navigableMap.floorEntry(2).toString());
        assertEquals(null, navigableMap.floorEntry(0));
    }

    @Test
    public void floorKey() {
        NavigableMap<Integer, Integer> navigableMap = new TreeMap<>();
        navigableMap.put(1, 1);
        navigableMap.put(2, 2);
        navigableMap.put(3, 3);
        navigableMap.put(4, 4);
        navigableMap.put(5, 5);
        assertEquals("2", navigableMap.floorKey(2).toString());
        assertEquals(null, navigableMap.floorKey(0));
    }

    @Test
    public void ceilingEntry() {
        NavigableMap<Integer, Integer> navigableMap = new TreeMap<>();
        navigableMap.put(1, 1);
        navigableMap.put(2, 2);
        navigableMap.put(3, 3);
        navigableMap.put(4, 4);
        navigableMap.put(5, 5);
        assertEquals("2=2", navigableMap.ceilingEntry(2).toString());
        assertEquals(null, navigableMap.ceilingEntry(6));
    }

    @Test
    public void ceilingKey() {
        NavigableMap<Integer, Integer> navigableMap = new TreeMap<>();
        navigableMap.put(1, 1);
        navigableMap.put(2, 2);
        navigableMap.put(3, 3);
        navigableMap.put(4, 4);
        navigableMap.put(5, 5);
        assertEquals("2", navigableMap.ceilingKey(2).toString());
        assertEquals(null, navigableMap.ceilingKey(6));
    }

    @Test
    public void higherEntry() {
        NavigableMap<Integer, Integer> navigableMap = new TreeMap<>();
        navigableMap.put(1, 1);
        navigableMap.put(2, 2);
        navigableMap.put(3, 3);
        navigableMap.put(4, 4);
        navigableMap.put(5, 5);
        assertEquals("3=3", navigableMap.higherEntry(2).toString());
        assertEquals(null, navigableMap.higherEntry(6));
    }

    @Test
    public void higherKey() {
        NavigableMap<Integer, Integer> navigableMap = new TreeMap<>();
        navigableMap.put(1, 1);
        navigableMap.put(2, 2);
        navigableMap.put(3, 3);
        navigableMap.put(4, 4);
        navigableMap.put(5, 5);
        assertEquals("3", navigableMap.higherKey(2).toString());
        assertEquals(null, navigableMap.higherKey(6));
    }

    @Test
    public void firstEntry() {
        NavigableMap<Integer, Integer> navigableMap = new TreeMap<>();
        assertEquals(null, navigableMap.firstEntry());
        navigableMap.put(1, 1);
        navigableMap.put(2, 2);
        navigableMap.put(3, 3);
        navigableMap.put(4, 4);
        navigableMap.put(5, 5);
        assertEquals("1=1", navigableMap.firstEntry().toString());
    }

    @Test
    public void lastEntry() {
        NavigableMap<Integer, Integer> navigableMap = new TreeMap<>();
        assertEquals(null, navigableMap.lastEntry());
        navigableMap.put(1, 1);
        navigableMap.put(2, 2);
        navigableMap.put(3, 3);
        navigableMap.put(4, 4);
        navigableMap.put(5, 5);
        assertEquals("5=5", navigableMap.lastEntry().toString());
    }

    @Test
    public void pollFirstEntry() {
        NavigableMap<Integer, Integer> navigableMap = new TreeMap<>();
        assertEquals(null, navigableMap.pollFirstEntry());
        navigableMap.put(1, 1);
        navigableMap.put(2, 2);
        navigableMap.put(3, 3);
        navigableMap.put(4, 4);
        navigableMap.put(5, 5);
        assertEquals("1=1", navigableMap.pollFirstEntry().toString());
    }

    @Test
    public void pollLastEntry() {
        NavigableMap<Integer, Integer> navigableMap = new TreeMap<>();
        assertEquals(null, navigableMap.pollLastEntry());
        navigableMap.put(1, 1);
        navigableMap.put(2, 2);
        navigableMap.put(3, 3);
        navigableMap.put(4, 4);
        navigableMap.put(5, 5);
        assertEquals("5=5", navigableMap.pollLastEntry().toString());
    }

    @Test
    public void descendingMap() {
        NavigableMap<Integer, Integer> navigableMap = new TreeMap<>();
        navigableMap.put(1, 1);
        navigableMap.put(2, 2);
        navigableMap.put(3, 3);
        navigableMap.put(4, 4);
        navigableMap.put(5, 5);
        assertEquals("{5=5, 4=4, 3=3, 2=2, 1=1}", navigableMap.descendingMap().toString());
    }

    @Test
    public void navigableKeySet() {
        NavigableMap<Integer, Integer> navigableMap = new TreeMap<>();
        navigableMap.put(1, 1);
        navigableMap.put(2, 2);
        navigableMap.put(3, 3);
        navigableMap.put(4, 4);
        navigableMap.put(5, 5);
        assertEquals("[1, 2, 3, 4, 5]", navigableMap.navigableKeySet().toString());
    }

    @Test
    public void descendingKeySet() {
        NavigableMap<Integer, Integer> navigableMap = new TreeMap<>();
        navigableMap.put(1, 1);
        navigableMap.put(2, 2);
        navigableMap.put(3, 3);
        navigableMap.put(4, 4);
        navigableMap.put(5, 5);
        assertEquals("[5, 4, 3, 2, 1]", navigableMap.descendingKeySet().toString());
    }

    @Test
    public void subMap() {
        NavigableMap<Integer, Integer> navigableMap = new TreeMap<>();
        navigableMap.put(1, 1);
        navigableMap.put(2, 2);
        navigableMap.put(3, 3);
        navigableMap.put(4, 4);
        navigableMap.put(5, 5);
        assertEquals("{1=1, 2=2, 3=3, 4=4, 5=5}", navigableMap.subMap(1, true, 5, true).toString());
        assertEquals("{1=1, 2=2, 3=3, 4=4}", navigableMap.subMap(1, true, 5, false).toString());
        assertEquals("{2=2, 3=3, 4=4, 5=5}", navigableMap.subMap(1, false, 5, true).toString());
        assertEquals("{2=2, 3=3, 4=4}", navigableMap.subMap(1, false, 5, false).toString());
    }

    @Test
    public void headMap() {
        NavigableMap<Integer, Integer> navigableMap = new TreeMap<>();
        navigableMap.put(1, 1);
        navigableMap.put(2, 2);
        navigableMap.put(3, 3);
        navigableMap.put(4, 4);
        navigableMap.put(5, 5);
        assertEquals("{1=1, 2=2, 3=3, 4=4, 5=5}", navigableMap.headMap(5, true).toString());
        assertEquals("{1=1, 2=2, 3=3, 4=4}", navigableMap.headMap(5, false).toString());
    }

    @Test
    public void tailMap() {
        NavigableMap<Integer, Integer> navigableMap = new TreeMap<>();
        navigableMap.put(1, 1);
        navigableMap.put(2, 2);
        navigableMap.put(3, 3);
        navigableMap.put(4, 4);
        navigableMap.put(5, 5);
        assertEquals("{1=1, 2=2, 3=3, 4=4, 5=5}", navigableMap.tailMap(1, true).toString());
        assertEquals("{2=2, 3=3, 4=4, 5=5}", navigableMap.tailMap(1, false).toString());
    }

    @Test(expected = NullPointerException.class)
    public void lowerEntryNullPointerException() {
        NavigableMap<Integer, Integer> navigableMap = new TreeMap<>();
        navigableMap.put(1, 1);
        navigableMap.put(2, 2);
        navigableMap.put(3, 3);
        navigableMap.put(4, 4);
        navigableMap.put(5, 5);
        navigableMap.lowerEntry(null);
    }

    @Test(expected = NullPointerException.class)
    public void lowerKeyNullPointerException() {
        NavigableMap<Integer, Integer> navigableMap = new TreeMap<>();
        navigableMap.put(1, 1);
        navigableMap.put(2, 2);
        navigableMap.put(3, 3);
        navigableMap.put(4, 4);
        navigableMap.put(5, 5);
        navigableMap.lowerKey(null);
    }

    @Test(expected = NullPointerException.class)
    public void floorEntryNullPointerException() {
        NavigableMap<Integer, Integer> navigableMap = new TreeMap<>();
        navigableMap.put(1, 1);
        navigableMap.put(2, 2);
        navigableMap.put(3, 3);
        navigableMap.put(4, 4);
        navigableMap.put(5, 5);
        navigableMap.floorEntry(null);
    }

    @Test(expected = NullPointerException.class)
    public void floorKeyNullPointerException() {
        NavigableMap<Integer, Integer> navigableMap = new TreeMap<>();
        navigableMap.put(1, 1);
        navigableMap.put(2, 2);
        navigableMap.put(3, 3);
        navigableMap.put(4, 4);
        navigableMap.put(5, 5);
        navigableMap.floorKey(null);
    }

    @Test(expected = NullPointerException.class)
    public void ceilingEntryNullPointerException() {
        NavigableMap<Integer, Integer> navigableMap = new TreeMap<>();
        navigableMap.put(1, 1);
        navigableMap.put(2, 2);
        navigableMap.put(3, 3);
        navigableMap.put(4, 4);
        navigableMap.put(5, 5);
        navigableMap.ceilingEntry(null);
    }

    @Test(expected = NullPointerException.class)
    public void ceilingKeyNullPointerException() {
        NavigableMap<Integer, Integer> navigableMap = new TreeMap<>();
        navigableMap.put(1, 1);
        navigableMap.put(2, 2);
        navigableMap.put(3, 3);
        navigableMap.put(4, 4);
        navigableMap.put(5, 5);
        navigableMap.ceilingKey(null);
    }

    @Test(expected = NullPointerException.class)
    public void higherEntryNullPointerException() {
        NavigableMap<Integer, Integer> navigableMap = new TreeMap<>();
        navigableMap.put(1, 1);
        navigableMap.put(2, 2);
        navigableMap.put(3, 3);
        navigableMap.put(4, 4);
        navigableMap.put(5, 5);
        navigableMap.higherEntry(null);
    }

    @Test(expected = NullPointerException.class)
    public void higherKeyNullPointerException() {
        NavigableMap<Integer, Integer> navigableMap = new TreeMap<>();
        navigableMap.put(1, 1);
        navigableMap.put(2, 2);
        navigableMap.put(3, 3);
        navigableMap.put(4, 4);
        navigableMap.put(5, 5);
        navigableMap.higherKey(null);
    }

    @Test(expected = NullPointerException.class)
    public void subMapNullPointerException() {
        NavigableMap<Integer, Integer> navigableMap = new TreeMap<>();
        navigableMap.put(1, 1);
        navigableMap.put(2, 2);
        navigableMap.put(3, 3);
        navigableMap.put(4, 4);
        navigableMap.put(5, 5);
        navigableMap.subMap(null, true, null, true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void subMapIllegalArgumentException() {
        NavigableMap<Integer, Integer> navigableMap = new TreeMap<>();
        navigableMap.put(1, 1);
        navigableMap.put(2, 2);
        navigableMap.put(3, 3);
        navigableMap.put(4, 4);
        navigableMap.put(5, 5);
        navigableMap.subMap(1, true, 5, true).subMap(1, true, 6, true);
    }

    @Test(expected = NullPointerException.class)
    public void headMapNullPointerException() {
        NavigableMap<Integer, Integer> navigableMap = new TreeMap<>();
        navigableMap.put(1, 1);
        navigableMap.put(2, 2);
        navigableMap.put(3, 3);
        navigableMap.put(4, 4);
        navigableMap.put(5, 5);
        navigableMap.headMap(null, true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void headMapIllegalArgumentException() {
        NavigableMap<Integer, Integer> navigableMap = new TreeMap<>();
        navigableMap.put(1, 1);
        navigableMap.put(2, 2);
        navigableMap.put(3, 3);
        navigableMap.put(4, 4);
        navigableMap.put(5, 5);
        navigableMap.subMap(1, true, 5, true).headMap(6, true);
    }

    @Test(expected = NullPointerException.class)
    public void tailMapNullPointerException() {
        NavigableMap<Integer, Integer> navigableMap = new TreeMap<>();
        navigableMap.put(1, 1);
        navigableMap.put(2, 2);
        navigableMap.put(3, 3);
        navigableMap.put(4, 4);
        navigableMap.put(5, 5);
        navigableMap.tailMap(null, true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void tailMapIllegalArgumentException() {
        NavigableMap<Integer, Integer> navigableMap = new TreeMap<>();
        navigableMap.put(1, 1);
        navigableMap.put(2, 2);
        navigableMap.put(3, 3);
        navigableMap.put(4, 4);
        navigableMap.put(5, 5);
        navigableMap.subMap(1, true, 5, true).tailMap(6, true);
    }
}
