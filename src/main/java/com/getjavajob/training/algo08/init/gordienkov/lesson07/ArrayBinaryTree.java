package com.getjavajob.training.algo08.init.gordienkov.lesson07;

import java.util.*;

/**
 * Concrete implementation of a binary tree using a node-based, linked structure
 *
 * @param <E> element
 */
public class ArrayBinaryTree<E> extends AbstractBinaryTree<E> {
    private static final int DEFAULT_CAPACITY = 18;
    private int size;
    private NodeImpl<E>[] tree;

    public ArrayBinaryTree() {
        tree = new NodeImpl[DEFAULT_CAPACITY];
    }

    protected NodeImpl<E> validate(Node<E> n) throws IllegalArgumentException {
        if (n instanceof NodeImpl) {
            return (NodeImpl<E>) n;
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public Node<E> left(Node<E> p) throws IllegalArgumentException {
        int index = getIndex(p);
        if (index == -1) {
            return null;
        } else {
            return tree[tree[index].leftIndex];
        }
    }

    @Override
    public Node<E> right(Node<E> p) throws IllegalArgumentException {
        int index = getIndex(p);
        if (index == -1) {
            return null;
        } else {
            return tree[tree[index].rightIndex];
        }
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        int index = getIndex(n);
        if (index == -1) {
            throw new IllegalArgumentException("Not existing Node");
        }
        int leftIndex = 2 * index + 1;
        if (leftIndex > tree.length) {
            increaseCapacity();
        }
        if (tree[index].leftIndex != null) {
            throw new IllegalArgumentException("Left Node already exists");
        } else {
            NodeImpl<E> node = new NodeImpl<>(e);
            node.parent = index;
            tree[leftIndex] = node;
            tree[index].leftIndex = leftIndex;
            size++;
            return node;
        }
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        int index = getIndex(n);
        if (index == -1) {
            throw new IllegalArgumentException("Not existing Node");
        }
        int rightIndex = 2 * index + 2;
        if (rightIndex > tree.length) {
            increaseCapacity();
        }
        if (tree[index].rightIndex != null) {
            throw new IllegalArgumentException("Right Node already exists");
        } else {
            NodeImpl<E> node = new NodeImpl<>(e);
            node.parent = index;
            tree[rightIndex] = node;
            tree[index].rightIndex = rightIndex;
            size++;
            return node;
        }
    }

    @Override
    public Node<E> root() {
        if (tree.length == 0) {
            return null;
        } else {
            return tree[0];
        }
    }

    @Override
    public Node<E> parent(Node<E> n) throws IllegalArgumentException {
        int index = getIndex(n);
        if (index == -1) {
            throw new IllegalArgumentException("Not existing Node");
        }
        if (index == 0) {
            throw new IllegalArgumentException("It's a root Node");
        }
        return tree[tree[index].parent];
    }

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        if (size != 0) {
            throw new IllegalStateException("Root Node already exists");
        }
        NodeImpl<E> root = new NodeImpl<>(e);
        tree[0] = root;
        size++;
        return root;
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        int index = getIndex(n);
        if (index == -1) {
            throw new IllegalArgumentException("Not existing Node");
        }
        if (tree[index].leftIndex != null & tree[index].rightIndex != null) {
            throw new IllegalArgumentException("Left Node and Right Node already exists");
        }
        if (tree[index].leftIndex != null) {
            int rightIndex = 2 * index + 2;
            if (rightIndex > tree.length) {
                increaseCapacity();
            }
            NodeImpl<E> rightNode = new NodeImpl<>(e);
            rightNode.parent = index;
            tree[rightIndex] = rightNode;
            tree[index].rightIndex = rightIndex;
            size++;
            return rightNode;
        } else {
            int leftIndex = 2 * index + 1;
            if (leftIndex > tree.length) {
                increaseCapacity();
            }
            NodeImpl<E> leftNode = new NodeImpl<>(e);
            leftNode.parent = index;
            tree[leftIndex] = leftNode;
            tree[index].leftIndex = leftIndex;
            size++;
            return leftNode;
        }
    }

    @Override
    public E set(Node<E> n, E e) throws IllegalArgumentException {
        int index = getIndex(n);
        if (index == -1) {
            throw new IllegalArgumentException("Not existing Node");
        }
        E oldValue = tree[index].element;
        tree[index].element = e;
        return oldValue;
    }

    @Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        int index = getIndex(n);
        if (index == -1) {
            throw new IllegalArgumentException("Not existing Node");
        }
        if (tree[index].leftIndex != null && tree[index].rightIndex != null) {
            throw new IllegalArgumentException("Node have two children");
        } else {
            if (tree[index].leftIndex != null && tree[index].rightIndex == null) {
                E element = tree[index].element;
                tree[index] = tree[tree[index].leftIndex];
                tree[2 * index + 1] = null;
                size--;
                return element;
            } else {
                E element = tree[index].element;
                if (Objects.equals(tree[index].parent, tree[tree[index].parent].leftIndex)) {
                    tree[tree[index].parent].leftIndex = null;
                    tree[index] = null;
                } else {
                    tree[tree[index].parent].rightIndex = null;
                }
                size--;
                return element;
            }
        }
    }

    @Override
    public boolean isInternal(Node<E> n) throws IllegalArgumentException {
        int index = getIndex(n);
        if (index == -1) {
            throw new IllegalArgumentException("Not existing Node");
        }
        return tree[index].rightIndex != null ^ tree[index].leftIndex != null;
    }

    @Override
    public boolean isExternal(Node<E> n) throws IllegalArgumentException {
        int index = getIndex(n);
        if (index == -1) {
            throw new IllegalArgumentException("Not existing Node");
        }
        return tree[index].rightIndex == null && tree[index].leftIndex == null;
    }

    @Override
    public Collection<Node<E>> preOrder() {
        if (size == 0) {
            return new ArrayList<>();
        } else {
            return preOrder(0, new ArrayList<Node<E>>());
        }
    }

    private Collection<Node<E>> preOrder(Integer index, Collection<Node<E>> collection) {
        if (index == null) {
            return collection;
        } else {
            collection.add(tree[index]);
            preOrder(tree[index].leftIndex, collection);
            preOrder(tree[index].rightIndex, collection);
        }
        return collection;
    }

    @Override
    public Collection<Node<E>> inOrder() {
        if (size == 0) {
            return new ArrayList<>();
        } else {
            return inOrder(0, new ArrayList<Node<E>>());
        }
    }

    private Collection<Node<E>> inOrder(Integer index, Collection<Node<E>> collection) {
        if (index == null) {
            return collection;
        } else {
            inOrder(tree[index].leftIndex, collection);
            collection.add(tree[index]);
            inOrder(tree[index].rightIndex, collection);
        }
        return collection;
    }

    @Override
    public Collection<Node<E>> postOrder() {
        if (size == 0) {
            return new ArrayList<>();
        } else {
            return postOrder(0, new ArrayList<Node<E>>());
        }
    }

    private Collection<Node<E>> postOrder(Integer index, Collection<Node<E>> collection) {
        if (index == null) {
            return collection;
        } else {
            postOrder(tree[index].leftIndex, collection);
            postOrder(tree[index].rightIndex, collection);
            collection.add(tree[index]);
        }
        return collection;
    }

    @Override
    public Collection<Node<E>> breadthFirst() {
        Collection<Node<E>> collection = new ArrayList<>();
        if (tree[0] == null) {
            return collection;
        }
        Stack<Node<E>> stack = new Stack<>();
        stack.push(tree[0]);
        while (!stack.empty()) {
            Node<E> n = stack.pop();
            int index = getIndex(n);
            collection.add(n);
            if (index != -1) {
                if (tree[index].rightIndex != null) {
                    stack.push(tree[tree[index].rightIndex]);
                }
                if (tree[index].leftIndex != null) {
                    stack.push(tree[tree[index].leftIndex]);
                }
            }
        }
        return collection;
    }

    @Override
    public Node<E> sibling(Node<E> n) throws IllegalArgumentException {
        int index = getIndex(n);
        if (index == -1) {
            throw new IllegalArgumentException("Not existing Node");
        }
        if (index == 0) {
            return null;
        } else {
            if (Objects.equals(tree[tree[index].parent].leftIndex, index)) {
                if (tree[tree[index].parent].rightIndex != null) {
                    return tree[tree[tree[index].parent].rightIndex];
                }
            } else {
                if (tree[tree[index].parent].leftIndex != null) {
                    return tree[tree[tree[index].parent].leftIndex];
                }
            }
        }
        return null;
    }

    @Override
    public Collection<Node<E>> children(Node<E> n) throws IllegalArgumentException {
        int index = getIndex(n);
        if (index == -1) {
            throw new IllegalArgumentException("Not existing Node");
        }
        Collection<Node<E>> collection = new ArrayList<>();
        if (tree[index].leftIndex != null && tree[index].rightIndex != null) {
            collection.add(tree[tree[index].leftIndex]);
            collection.add(tree[tree[index].rightIndex]);
        } else if (tree[index].leftIndex != null && tree[index].rightIndex == null) {
            collection.add(tree[tree[index].leftIndex]);
        } else if (tree[index].leftIndex == null && tree[index].rightIndex != null) {
            collection.add(tree[tree[index].rightIndex]);
        } else {
            return collection;
        }
        return collection;
    }

    @Override
    public int childrenNumber(Node<E> n) throws IllegalArgumentException {
        int index = getIndex(n);
        if (index == -1) {
            throw new IllegalArgumentException("Not existing Node");
        }
        if (tree[index].leftIndex != null && tree[index].rightIndex != null) {
            return 2;
        } else if (tree[index].leftIndex != null || tree[index].rightIndex != null) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<E> iterator() {
        return new Itr();
    }

    @Override
    public Collection<Node<E>> nodes() {
        Collection<Node<E>> collection = new ArrayList<>();
        for (NodeImpl<E> node : tree) {
            if (node != null) {
                collection.add(node);
            }
        }
        return collection;
    }

    private int getIndex(Node<E> n) {
        for (int i = 0; i < tree.length; i++) {
            if (n.equals(tree[i])) {
                return i;
            }
        }
        return -1;
    }

    private void increaseCapacity() {
        NodeImpl<E>[] temp = new NodeImpl[tree.length * 2];
        System.arraycopy(tree, 0, temp, 0, tree.length);
        tree = temp;
    }

    protected static class NodeImpl<E> implements Node<E> {
        E element;
        Integer leftIndex;
        Integer rightIndex;
        Integer parent;

        public NodeImpl(E element) {
            this.element = element;
        }

        @Override
        public E getElement() {
            return element;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof NodeImpl)) return false;
            NodeImpl<?> node = (NodeImpl<?>) o;
            return Objects.equals(getElement(), node.getElement()) &&
                    Objects.equals(leftIndex, node.leftIndex) &&
                    Objects.equals(rightIndex, node.rightIndex) &&
                    Objects.equals(parent, node.parent);
        }

        @Override
        public int hashCode() {
            return Objects.hash(getElement(), leftIndex, rightIndex, parent);
        }

        @Override
        public String toString() {
            if (element == null) {
                return null;
            } else {
                return element.toString();
            }
        }
    }

    private class Itr implements Iterator<E> {
        private Iterator<Node<E>> it = nodes().iterator();

        @Override
        public boolean hasNext() {
            return it.hasNext();
        }

        @Override
        public E next() {
            return it.next().getElement();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}