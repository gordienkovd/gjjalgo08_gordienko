package com.getjavajob.training.algo08.init.gordienkov.lesson06;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class MatrixExpr implements Matrix {
    private Map<Indexes, Object> data;

    public MatrixExpr() {
        data = new HashMap<>();
    }

    @Override
    public Object get(int i, int j) {
        return data.get(new Indexes(i, j));
    }

    @Override
    public void set(int i, int j, Object value) {
        data.put(new Indexes(i, j), value);
    }

    private static class Indexes {
        int i;
        int j;

        public Indexes(int i, int j) {
            this.i = i;
            this.j = j;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Indexes)) return false;
            Indexes indexes = (Indexes) o;
            return i == indexes.i &&
                    j == indexes.j;
        }

        @Override
        public int hashCode() {
            return Objects.hash(i, j);
        }
    }
}
