package com.getjavajob.training.algo08.init.gordienkov.lesson05;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class LinkedListStackTest {
    @Test
    public void push() throws Exception {
        LinkedListStack<Integer> actual = new LinkedListStack<>();
        actual.add(1);
        actual.push(2);
        actual.push(3);
        actual.push(4);
        Integer[] expected = new Integer[]{4, 3, 2, 1};
        assertArrayEquals(expected, actual.asList().toArray());
    }

    @Test
    public void pop() throws Exception {
        LinkedListStack<Integer> actual = new LinkedListStack<>();
        actual.add(1);
        actual.add(2);
        actual.pop();
        Integer[] expected = new Integer[]{2};
        assertArrayEquals(expected, actual.asList().toArray());
    }

}