package com.getjavajob.training.algo08.init.gordienkov.lesson10;

import org.junit.Test;

import static com.getjavajob.training.algo08.init.gordienkov.lesson10.MergeSort.mergeSort;
import static org.junit.Assert.assertArrayEquals;

public class MergeSortTest {
    @Test
    public void mergeSortTest() throws Exception {
        Integer[] expected = new Integer[Integer.MAX_VALUE / 4];
        for (int i = 0; i < expected.length; i++) {
            expected[i] = expected.length - i;
        }
        Integer[] actual = new Integer[Integer.MAX_VALUE / 4];
        for (int i = 0; i < actual.length; i++) {
            actual[i] = i + 1;
        }
        mergeSort(expected);
        assertArrayEquals(actual, expected);
    }
}