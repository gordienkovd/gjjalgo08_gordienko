package com.getjavajob.training.algo08.init.gordienkov.lesson05;

public interface Closure<O> {
    void execute(O input);
}
