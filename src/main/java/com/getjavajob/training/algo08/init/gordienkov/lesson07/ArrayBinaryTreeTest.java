package com.getjavajob.training.algo08.init.gordienkov.lesson07;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Виталий on 10.10.2016.
 */
public class ArrayBinaryTreeTest {
    @Test
    public void left() throws Exception {
        BinaryTree<String> arrayBinaryTree = new ArrayBinaryTree<>();
        arrayBinaryTree.addRoot("a");
        arrayBinaryTree.addLeft(arrayBinaryTree.root(), "b");
        assertEquals("b", arrayBinaryTree.left(arrayBinaryTree.root()).getElement());
    }

    @Test
    public void right() throws Exception {
        BinaryTree<String> arrayBinaryTree = new ArrayBinaryTree<>();
        arrayBinaryTree.addRoot("a");
        arrayBinaryTree.addRight(arrayBinaryTree.root(), "b");
        assertEquals("b", arrayBinaryTree.right(arrayBinaryTree.root()).getElement());
    }

    @Test
    public void addLeft() throws Exception {
        BinaryTree<String> arrayBinaryTree = new ArrayBinaryTree<>();
        arrayBinaryTree.addRoot("a");
        assertEquals("b", arrayBinaryTree.addLeft(arrayBinaryTree.root(), "b").getElement());
    }

    @Test
    public void addRight() throws Exception {
        BinaryTree<String> arrayBinaryTree = new ArrayBinaryTree<>();
        arrayBinaryTree.addRoot("a");
        assertEquals("b", arrayBinaryTree.addRight(arrayBinaryTree.root(), "b").getElement());
    }

    @Test
    public void root() throws Exception {
        BinaryTree<String> arrayBinaryTree = new ArrayBinaryTree<>();
        arrayBinaryTree.addRoot("a");
        assertEquals("a", arrayBinaryTree.root().getElement());
    }

    @Test
    public void parent() throws Exception {
        BinaryTree<String> arrayBinaryTree = new ArrayBinaryTree<>();
        arrayBinaryTree.addRoot("a");
        Node<String> left = arrayBinaryTree.addLeft(arrayBinaryTree.root(), "b");
        arrayBinaryTree.addRight(arrayBinaryTree.root(), "c");
        assertEquals("a", arrayBinaryTree.parent(left).getElement());
    }

    @Test
    public void addRoot() throws Exception {
        BinaryTree<String> arrayBinaryTree = new ArrayBinaryTree<>();
        assertEquals("a", arrayBinaryTree.addRoot("a").getElement());
    }

    @Test
    public void add() throws Exception {
        BinaryTree<String> arrayBinaryTree = new ArrayBinaryTree<>();
        arrayBinaryTree.addRoot("a");
        assertEquals("b", arrayBinaryTree.add(arrayBinaryTree.root(), "b").getElement());
    }

    @Test
    public void set() throws Exception {
        BinaryTree<String> arrayBinaryTree = new ArrayBinaryTree<>();
        arrayBinaryTree.addRoot("a");
        Node<String> left = arrayBinaryTree.addLeft(arrayBinaryTree.root(), "b");
        assertEquals("b", arrayBinaryTree.set(left, "c"));
    }

    @Test
    public void remove() throws Exception {
        BinaryTree<String> arrayBinaryTree = new ArrayBinaryTree<>();
        arrayBinaryTree.addRoot("a");
        Node<String> left = arrayBinaryTree.addLeft(arrayBinaryTree.root(), "b");
        assertEquals("b", arrayBinaryTree.remove(left));
    }

    @Test
    public void isInternal() throws Exception {
        BinaryTree<String> arrayBinaryTree = new ArrayBinaryTree<>();
        arrayBinaryTree.addRoot("a");
        assertFalse(arrayBinaryTree.isInternal(arrayBinaryTree.root()));
        arrayBinaryTree.add(arrayBinaryTree.root(), "b");
        assertTrue(arrayBinaryTree.isInternal(arrayBinaryTree.root()));
        arrayBinaryTree.add(arrayBinaryTree.root(), "c");
        assertFalse(arrayBinaryTree.isInternal(arrayBinaryTree.root()));
    }

    @Test
    public void isExternal() throws Exception {
        BinaryTree<String> arrayBinaryTree = new ArrayBinaryTree<>();
        arrayBinaryTree.addRoot("a");
        assertTrue(arrayBinaryTree.isExternal(arrayBinaryTree.root()));
        arrayBinaryTree.add(arrayBinaryTree.root(), "b");
        assertFalse(arrayBinaryTree.isExternal(arrayBinaryTree.root()));
        arrayBinaryTree.add(arrayBinaryTree.root(), "c");
        assertFalse(arrayBinaryTree.isExternal(arrayBinaryTree.root()));
    }

    @Test
    public void isEmpty() throws Exception {
        BinaryTree<String> arrayBinaryTree = new ArrayBinaryTree<>();
        assertTrue(arrayBinaryTree.isEmpty());
        arrayBinaryTree.addRoot("a");
        assertFalse(arrayBinaryTree.isEmpty());
    }

    @Test
    public void preOrder() throws Exception {
        ArrayBinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("a");
        Node<String> node = tree.add(tree.root(), "b");
        Node<String> node1 = tree.add(tree.root(), "c");
        tree.add(node, "d");
        tree.add(node, "e");
        tree.add(node1, "f");
        tree.add(node1, "g");
        assertEquals("[a, b, d, e, c, f, g]", tree.preOrder().toString());
    }

    @Test
    public void inOrder() throws Exception {
        ArrayBinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("a");
        Node<String> node = tree.add(tree.root(), "b");
        Node<String> node1 = tree.add(tree.root(), "c");
        tree.add(node, "d");
        tree.add(node, "e");
        tree.add(node1, "f");
        tree.add(node1, "g");
        assertEquals("[d, b, e, a, f, c, g]", tree.inOrder().toString());
    }

    @Test
    public void postOrder() throws Exception {
        ArrayBinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("a");
        Node<String> node = tree.add(tree.root(), "b");
        Node<String> node1 = tree.add(tree.root(), "c");
        tree.add(node, "d");
        tree.add(node, "e");
        tree.add(node1, "f");
        tree.add(node1, "g");
        assertEquals("[d, e, b, f, g, c, a]", tree.postOrder().toString());
    }

    @Test
    public void breadthFirst() throws Exception {
        ArrayBinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("a");
        Node<String> node = tree.add(tree.root(), "b");
        Node<String> node1 = tree.add(tree.root(), "c");
        tree.add(node, "d");
        tree.add(node, "e");
        tree.add(node1, "f");
        tree.add(node1, "g");
        assertEquals("[a, b, d, e, c, f, g]", tree.breadthFirst().toString());
    }

    @Test
    public void sibling() throws Exception {
        ArrayBinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("a");
        Node<String> node = tree.add(tree.root(), "b");
        Node<String> node1 = tree.add(tree.root(), "c");
        tree.add(node, "d");
        tree.add(node, "e");
        Node<String> node3 = tree.add(node1, "g");
        assertEquals("b", tree.sibling(node1).getElement());
        assertEquals("c", tree.sibling(node).getElement());
        assertEquals(null, tree.sibling(node3));
    }

    @Test
    public void children() throws Exception {
        ArrayBinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("a");
        Node<String> node = tree.add(tree.root(), "b");
        Node<String> node1 = tree.add(tree.root(), "c");
        tree.add(node, "d");
        tree.add(node, "e");
        tree.add(node1, "f");
        tree.add(node1, "g");
        assertEquals("[d, e]", tree.children(node).toString());
    }

    @Test
    public void childrenNumber() throws Exception {
        ArrayBinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("a");
        Node<String> node = tree.add(tree.root(), "b");
        Node<String> node1 = tree.add(tree.root(), "c");
        tree.add(node, "d");
        tree.add(node, "e");
        Node<String> node3 = tree.add(node1, "f");
        assertEquals(2, tree.childrenNumber(node));
        assertEquals(1, tree.childrenNumber(node1));
        assertEquals(0, tree.childrenNumber(node3));
    }

    @Test
    public void size() throws Exception {
        BinaryTree<String> arrayBinaryTree = new ArrayBinaryTree<>();
        arrayBinaryTree.addRoot("a");
        arrayBinaryTree.addLeft(arrayBinaryTree.root(), "b");
        assertEquals(2, arrayBinaryTree.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void addLeftIllegalArgumentException() {
        BinaryTree<String> arrayBinaryTree = new ArrayBinaryTree<>();
        arrayBinaryTree.addRoot("a");
        arrayBinaryTree.addLeft(new ArrayBinaryTree.NodeImpl<>("b"), "c");
    }

    @Test(expected = IllegalArgumentException.class)
    public void addLeftIllegalArgumentException2() {
        BinaryTree<String> arrayBinaryTree = new ArrayBinaryTree<>();
        arrayBinaryTree.addRoot("a");
        arrayBinaryTree.addLeft(arrayBinaryTree.root(), "b");
        arrayBinaryTree.addLeft(arrayBinaryTree.root(), "c");
    }

    @Test(expected = IllegalArgumentException.class)
    public void addRightIllegalArgumentException() {
        BinaryTree<String> arrayBinaryTree = new ArrayBinaryTree<>();
        arrayBinaryTree.addRoot("a");
        arrayBinaryTree.addRight(new ArrayBinaryTree.NodeImpl<>("b"), "c");
    }

    @Test(expected = IllegalArgumentException.class)
    public void addRightIllegalArgumentException2() {
        BinaryTree<String> arrayBinaryTree = new ArrayBinaryTree<>();
        arrayBinaryTree.addRoot("a");
        arrayBinaryTree.addRight(arrayBinaryTree.root(), "b");
        arrayBinaryTree.addRight(arrayBinaryTree.root(), "c");
    }

    @Test(expected = IllegalArgumentException.class)
    public void parentIllegalArgumentException() {
        BinaryTree<String> arrayBinaryTree = new ArrayBinaryTree<>();
        arrayBinaryTree.addRoot("a");
        arrayBinaryTree.parent(arrayBinaryTree.root());
    }

    @Test(expected = IllegalArgumentException.class)
    public void parentIllegalArgumentException2() {
        BinaryTree<String> arrayBinaryTree = new ArrayBinaryTree<>();
        arrayBinaryTree.addRoot("a");
        arrayBinaryTree.parent(new ArrayBinaryTree.NodeImpl<>("b"));
    }

    @Test(expected = IllegalStateException.class)
    public void addRootIllegalStateException() {
        BinaryTree<String> arrayBinaryTree = new ArrayBinaryTree<>();
        arrayBinaryTree.addRoot("a");
        arrayBinaryTree.addRoot("b");
    }

    @Test(expected = IllegalArgumentException.class)
    public void addIllegalArgumentException() {
        BinaryTree<String> arrayBinaryTree = new ArrayBinaryTree<>();
        arrayBinaryTree.addRoot("a");
        arrayBinaryTree.add(new ArrayBinaryTree.NodeImpl<>("b"), "c");
    }

    @Test(expected = IllegalArgumentException.class)
    public void addIllegalArgumentException2() {
        BinaryTree<String> arrayBinaryTree = new ArrayBinaryTree<>();
        arrayBinaryTree.addRoot("a");
        arrayBinaryTree.addLeft(arrayBinaryTree.root(), "b");
        arrayBinaryTree.addRight(arrayBinaryTree.root(), "c");
        arrayBinaryTree.add(new ArrayBinaryTree.NodeImpl<>("b"), "c");
    }

    @Test(expected = IllegalArgumentException.class)
    public void setIllegalArgumentException() {
        BinaryTree<String> arrayBinaryTree = new ArrayBinaryTree<>();
        arrayBinaryTree.addRoot("a");
        arrayBinaryTree.set(new ArrayBinaryTree.NodeImpl<>("b"), "c");
    }

    @Test(expected = IllegalArgumentException.class)
    public void removeIllegalArgumentException() {
        BinaryTree<String> arrayBinaryTree = new ArrayBinaryTree<>();
        arrayBinaryTree.addRoot("a");
        arrayBinaryTree.remove(new ArrayBinaryTree.NodeImpl<>("b"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void removeIllegalArgumentException2() {
        BinaryTree<String> arrayBinaryTree = new ArrayBinaryTree<>();
        arrayBinaryTree.addRoot("a");
        arrayBinaryTree.addLeft(arrayBinaryTree.root(), "b");
        arrayBinaryTree.addRight(arrayBinaryTree.root(), "c");
        arrayBinaryTree.remove(arrayBinaryTree.root());
    }

    @Test(expected = IllegalArgumentException.class)
    public void isInternalIllegalArgumentException() {
        BinaryTree<String> arrayBinaryTree = new ArrayBinaryTree<>();
        arrayBinaryTree.addRoot("a");
        arrayBinaryTree.isInternal(new ArrayBinaryTree.NodeImpl<>("b"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void isExternalIllegalArgumentException() {
        BinaryTree<String> arrayBinaryTree = new ArrayBinaryTree<>();
        arrayBinaryTree.addRoot("a");
        arrayBinaryTree.isExternal(new ArrayBinaryTree.NodeImpl<>("b"));
    }
}