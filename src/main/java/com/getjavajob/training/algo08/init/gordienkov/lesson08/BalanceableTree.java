package com.getjavajob.training.algo08.init.gordienkov.lesson08;

import com.getjavajob.training.algo08.init.gordienkov.lesson07.Node;

public abstract class BalanceableTree<E> extends BinarySearchTree<E> {

    /**
     * Sets new relationship between parent and child. This method is used by
     * {@link #rotate(com.getjavajob.training.algo08.init.gordienkov.lesson07.Node)} for node and its grandparent,
     * node and its parent, node's child and node's parent relinking.
     *
     * @param parent        new parent
     * @param child         new child
     * @param makeLeftChild whether new child must be left or right
     */
    private void relink(NodeImpl<E> parent, NodeImpl<E> child, boolean makeLeftChild) {
        if (parent == null) {
            return;
        }
        if (makeLeftChild) {
            parent.setLeft(child);
        } else {
            parent.setRight(child);
        }
        if (child != null) {
            child.setParent(parent);
        }
    }

    /**
     * Rotates n with it's parent.
     *
     * @param n node to rotate above its parent
     */
    protected void rotate(Node<E> n) {
        NodeImpl<E> p = validate(parent(n));
        if (n == null || p == null) {
            return;
        }
        boolean leftRotate = compare(n.getElement(), p.getElement()) > 0;
        if (leftRotate) {
            relink(p, validate(left(n)), false);
        } else {
            relink(p, validate(right(n)), true);
        }
        NodeImpl<E> g = validate(parent(p)); // grandparent of 'n'-node
        if (g != null) {
            relink(g, validate(n), p == left(g));
        } else {
            validate(n).setParent(null);
            root = validate(n);
        }
        relink(validate(n), p, leftRotate);
    }

    /**
     * Performs one rotation of <i>n</i>'s parent node or two rotations of <i>n</i> by the means of
     * {@link #rotate(com.getjavajob.training.algo08.init.gordienkov.lesson07.Node)} to reduce the height of subtree rooted at <i>n1</i>
     * <p>
     * <pre>
     *     n1         n2           n1           n
     *    /          /  \         /            / \
     *   n2    ==>  n   n1  or  n2     ==>   n2   n1
     *  /                         \
     * n                           n
     * </pre>
     * <p>
     * Similarly for subtree with right side children.
     *
     * @param n grand child of subtree root node
     * @return new subtree root
     */
    protected Node<E> reduceSubtreeHeight(Node<E> n) {
        if (n == null) {
            return null;
        }
        Node<E> p = parent(n);
        if (compare(n.getElement(), p.getElement()) * compare(p.getElement(), parent(p).getElement()) > 0) {
            rotate(p);
            return p;
        } else {
            rotate(n);
            rotate(n);
            return n;
        }
    }

    /**
     * Implement toString to represent a tree like (also each level can be a separate line):
     * (n1(n11(n111, n112), n12(n121, n122)))
     * that corresponds to tree hierarchy:
     * <p>
     * n1
     * n11          n12
     * n111 n112   n121   n122
     */
    @Override
    public String toString() {
        return toString(root());
    }

    private String toString(Node<E> node) {
        if (node == null) {
            return "";
        } else {
            return "(" + node + toString(left(node)) + toString(right(node)) + ")";
        }
    }
}
