package com.getjavajob.training.algo08.init.gordienkov.lesson06;

public class AssociativeArray<K, V> {
    private int size;
    private static final int DEFAULT_CAPACITY = 16;
    private Elem<K, V>[] data;

    public AssociativeArray() {
        this(DEFAULT_CAPACITY);
    }

    public AssociativeArray(int capacity) {
        data = new Elem[capacity];
    }

    public V add(K key, V value) {
        if (data.length == size) {
            resize();
        }
        V oldValue = null;
        int newIndex = getNewIndex(key);
        if (data[newIndex] == null) {
            data[newIndex] = new Elem<>(key, value, newIndex);
            size++;
        } else {
            if (equalsKeys(data[newIndex].key, key)) {
                oldValue = data[newIndex].value;
                data[newIndex].value = value;
            } else {
                Elem<K, V> e = data[newIndex];
                while (true) {
                    if (e.next == null) {
                        e.next = new Elem<>(key, value, newIndex);
                        size++;
                        break;
                    } else if (equalsKeys(e.next.key, key)) {
                        e.next.value = value;
                        break;
                    } else {
                        e = e.next;
                    }
                }
            }
        }
        return oldValue;
    }

    public V remove(K key) {
        if (data.length != 0) {
            V oldValue = null;
            int newIndex = getNewIndex(key);
            if (data[newIndex] != null && data[newIndex].next == null) {
                oldValue = data[newIndex].value;
                data[newIndex] = null;
                size--;
                return oldValue;
            } else if (data[newIndex] != null) {
                if (data[newIndex].key == key || data[newIndex].key.equals(key)) {
                    oldValue = data[newIndex].value;
                    data[newIndex] = data[newIndex].next;
                    size--;
                    return oldValue;
                } else {
                    Elem<K, V> e = data[newIndex];
                    while (true) {
                        if (e.next != null) {
                            if (e.next.key == key || e.next.key.equals(key)) {
                                oldValue = e.next.value;
                                if (e.next.next != null) {
                                    e.next = e.next.next;
                                    size--;
                                    break;
                                } else {
                                    e.next = null;
                                    size--;
                                    break;
                                }
                            } else {
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                }
            }
            return oldValue;
        } else {
            return null;
        }
    }

    public V get(K key) {
        int hash = getNewIndex(key);
        if (data.length != 0) {
            if (data[hash].key == key || data[hash].key.equals(key)) {
                return data[hash].value;
            } else if (data[hash].next != null) {
                Elem<K, V> e = data[hash];
                while (true) {
                    if (e.next != null) {
                        if (e.next.key == key || e.next.key.equals(key)) {
                            return e.next.value;
                        }
                        e = e.next;
                    }
                }
            }
        }
        return null;
    }

    public int getNewIndex(K key) {
        int newIndex;
        if (data.length != 0) {
            if (key == null) {
                newIndex = "null".hashCode() % data.length;
            } else {
                newIndex = key.hashCode() % data.length;
            }
        } else {
            newIndex = 0;
        }
        if (newIndex < 0) {
            return -newIndex;
        } else {
            return newIndex;
        }
    }

    private void resize() {
        if (data.length < 2) {
            data = new Elem[DEFAULT_CAPACITY];
        } else {
            Elem<K, V>[] oldData = data;
            data = new Elem[data.length * 2];
            size = 0;
            transferElements(oldData);
        }
    }

    private void transferElements(Elem<K, V>[] oldData) {
        for (Elem<K, V> anOldData : oldData) {
            if (anOldData != null) {
                Elem<K, V> e = anOldData;
                while (true) {
                    if (e.next == null) {
                        add(e.key, e.value);
                        break;
                    } else {
                        add(e.key, e.value);
                        e = e.next;
                    }
                }
            }
        }
    }

    private boolean equalsKeys(K key1, K key2) {
        if (key1 == null || key2 == null) {
            return key1 == key2;
        } else {
            return key1.equals(key2);
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (Elem<K, V> aData : data) {
            if (aData != null) {
                builder.append(aData.toString());
            }
        }
        return builder.toString();
    }

    private static class Elem<K, V> {
        private Elem<K, V> next;
        private V value;
        private K key;
        private int hash;

        Elem(K key, V value, int hash) {
            this.key = key;
            this.value = value;
            this.hash = hash;
        }

        @Override
        public String toString() {
            StringBuilder builder = new StringBuilder();
            Elem<K, V> e = this;
            builder.append('[').append(e.key).append('=');
            builder.append(e.value).append(" ").append(hash).append(']');
            while (e.next != null) {
                e = e.next;
                builder.append('[').append(e.key).append('=');
                builder.append(e.value).append(" ").append(hash).append(']');
            }
            return builder.toString();
        }
    }
}
