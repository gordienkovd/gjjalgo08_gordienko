package com.getjavajob.training.algo08.init.gordienkov.lesson09;

import com.getjavajob.training.algo08.init.gordienkov.lesson07.Node;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RedBlackTreeTest {
    @Test
    public void validate() throws Exception {
        assertEquals(null, new RedBlackTree<String>().validate(null));
        assertEquals("1", new RedBlackTree<Integer>().validate(new RedBlackTree.NodeRBT<>(1, null, null, null)).getElement().toString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void validateIllegalArgumentException() {
        new RedBlackTree<String>().validate(new Node<String>() {
            @Override
            public String getElement() {
                return null;
            }
        });
    }

    @Test
    public void addRoot() throws Exception {
        RedBlackTree<String> redBlackTree = new RedBlackTree<>();
        assertEquals("Moscow", redBlackTree.addRoot("Moscow").getElement());
    }

    @Test(expected = IllegalStateException.class)
    public void addRootIllegalStateException() {
        RedBlackTree<String> redBlackTree = new RedBlackTree<>();
        redBlackTree.addRoot("Moscow");
        redBlackTree.addRoot("Tver");
    }

    @Test
    public void add() throws Exception {
        RedBlackTree<String> redBlackTree = new RedBlackTree<>();
        Node<String> root = redBlackTree.addRoot("Moscow");
        assertEquals("Tver", redBlackTree.add(root, "Tver").getElement());
    }

    @Test(expected = IllegalArgumentException.class)
    public void addIllegalArgumentException() {
        RedBlackTree<String> redBlackTree = new RedBlackTree<>();
        Node<String> root = redBlackTree.addRoot("Moscow");
        redBlackTree.add(root, "Tver");
        redBlackTree.add(root, "Samara");
        redBlackTree.add(root, "Omsk");
    }

    @Test
    public void addLeft() throws Exception {
        RedBlackTree<String> redBlackTree = new RedBlackTree<>();
        Node<String> root = redBlackTree.addRoot("Moscow");
        assertEquals("Tver", redBlackTree.addLeft(root, "Tver").getElement());
    }

    @Test(expected = IllegalArgumentException.class)
    public void addLeftIllegalArgumentException() throws Exception {
        RedBlackTree<String> redBlackTree = new RedBlackTree<>();
        Node<String> root = redBlackTree.addRoot("Moscow");
        redBlackTree.addLeft(root, "Tver");
        redBlackTree.addLeft(root, "Omsk");
    }

    @Test
    public void addRight() throws Exception {
        RedBlackTree<String> redBlackTree = new RedBlackTree<>();
        Node<String> root = redBlackTree.addRoot("Moscow");
        assertEquals("Tver", redBlackTree.addRight(root, "Tver").getElement());
    }

    @Test(expected = IllegalArgumentException.class)
    public void addRightIllegalArgumentException() throws Exception {
        RedBlackTree<String> redBlackTree = new RedBlackTree<>();
        Node<String> root = redBlackTree.addRoot("Moscow");
        redBlackTree.addRight(root, "Tver");
        redBlackTree.addRight(root, "Omsk");
    }

    @Test
    public void remove() throws Exception {
        RedBlackTree<String> redBlackTree = new RedBlackTree<>();
        Node<String> root = redBlackTree.addRoot("Moscow");
        Node<String> node = redBlackTree.addRight(root, "Tver");
        assertEquals("Tver", redBlackTree.remove(node));
    }
}