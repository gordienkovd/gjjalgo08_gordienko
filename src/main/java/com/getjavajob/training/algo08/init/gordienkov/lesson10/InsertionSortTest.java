package com.getjavajob.training.algo08.init.gordienkov.lesson10;

import org.junit.Test;

import static com.getjavajob.training.algo08.init.gordienkov.lesson10.InsertionSort.insertionSort;
import static org.junit.Assert.assertArrayEquals;

public class InsertionSortTest {
    @Test
    public void insertionSortTest() throws Exception {
        int[] expected = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        int[] actual = new int[]{3, 5, 1, 2, 4, 7, 9, 6, 8};
        insertionSort(actual);
        assertArrayEquals(expected, actual);
    }

}